#include <Arduino.h>
#include "LCDHAL.h"
#include "PINOUT.h"

LCDHAL* LCDHAL::_Instance = NULL;

int LCDHAL::_Putchar(char c, FILE *stream)
{
  _Instance->Putchar(c);
  return 0;
}

LCDHAL::LCDHAL(void)
{
  if (!_Instance)
  {
    _Instance = this;
    fdev_setup_stream(&_LCDOut,&_Instance->_Putchar,NULL,_FDEV_SETUP_WRITE);
    _Oristdout = *stdout;
    stdout = &_LCDOut;
  }
}

LCDHAL::~LCDHAL(void)
{
  stdout = &_Oristdout;
  _Instance = NULL;
}

void LCDHAL::Update(void)
{
  if(!BufferEmpty()) _Print(_BufferPop());
}

bool LCDHAL::Init(void)
{
  sLCDBuffer Request;

  bitWrite(LCD_RS_DDR,LCD_RS_BIT,1);
  bitWrite(LCD_RW_DDR,LCD_RW_BIT,1);
  bitWrite(LCD_E_DDR,LCD_E_BIT,1);
  bitWrite(LCD_RS_PORT,LCD_RS_BIT,1);
  bitWrite(LCD_RW_PORT,LCD_RW_BIT,0);
  bitWrite(LCD_E_PORT,LCD_E_BIT,0);
  LCD_DATA_DDR=0xFF;

  Request.Command=1;
  Request.Data=0x38;
  if(!_BufferPush(&Request)) return false;
  Request.Data=0x0C;
  if(!_BufferPush(&Request)) return false;
  Request.Data=0x06;
  if(_BufferPush(&Request)) return false;
  return true;
}

bool LCDHAL::Clear(void)
{
  sLCDBuffer Request;

  Request.Command=1;
  Request.Data=0x01;
  if(!_BufferPush(&Request)) return false;
  else return true;
}

void LCDHAL::_Print(sLCDBuffer *Request)        // subroutine for lcd data
{
  LCD_DATA_PORT = Request->Data;                  // data byte to lcd
  if(Request->Command) bitWrite(LCD_RS_PORT,LCD_RS_BIT,0);          //Set RS Low
  bitWrite(LCD_E_PORT,LCD_E_BIT,1);          //Toggle E
  bitWrite(LCD_E_PORT,LCD_E_BIT,0);
  if(Request->Command) bitWrite(LCD_RS_PORT,LCD_RS_BIT,1);          //Set RS High
}

bool LCDHAL::Goto(unsigned char Column, unsigned char Row)
{
	if((Row>=4)||(Column>=20)||_Flags.BufferFull) return false;
	else
  {
    _Buffer[_BufferInputIndex].Command=1;
    _Buffer[_BufferInputIndex].Data=Column+_RowOffset[Row];
    if(++_BufferInputIndex>=BufferSize)_BufferInputIndex=0;
    if(_BufferInputIndex==_BufferOutputIndex) _Flags.BufferFull=true;
    _Flags.BufferEmpty=false;
    return true;
  }
}

bool LCDHAL::Putchar(char Character)
{
  if(_Flags.BufferFull) return false;
  else
  {
    _Buffer[_BufferInputIndex].Command=0;
    _Buffer[_BufferInputIndex].Data=Character;
    if(++_BufferInputIndex>=BufferSize)_BufferInputIndex=0;
    if(_BufferInputIndex==_BufferOutputIndex) _Flags.BufferFull=true;
    _Flags.BufferEmpty=false;
    return true;
  }
}

bool LCDHAL::_BufferPush(sLCDBuffer *LCDCommand)
{
  if(_Flags.BufferFull) return false;
  else
  {
    _Buffer[_BufferInputIndex]=*LCDCommand;
    if(++_BufferInputIndex>=BufferSize)_BufferInputIndex=0;
    _Flags.BufferEmpty=false;
    if(_BufferInputIndex==_BufferOutputIndex) _Flags.BufferFull=true;
    return true;
  }
}

sLCDBuffer* LCDHAL::_BufferPop(void)
{
  sLCDBuffer *LCDCommand=&_Buffer[_BufferOutputIndex];
  if(++_BufferOutputIndex>=BufferSize) _BufferOutputIndex=0;
  if(_BufferOutputIndex==_BufferInputIndex) _Flags.BufferEmpty=true;
  _Flags.BufferFull=false;
  return LCDCommand;
}

bool LCDHAL::Write(const char *OutputString,unsigned char Size)
{
  while(Size--)
  {
    _Buffer[_BufferInputIndex].Command=0;
    _Buffer[_BufferInputIndex].Data=*(unsigned char*)OutputString++;
    if(++_BufferInputIndex>=BufferSize)_BufferInputIndex=0;
    _Flags.BufferEmpty=false;
    if(_BufferInputIndex==_BufferOutputIndex)
    {
      _Flags.BufferFull=true;
      if(Size!=0)return(false);
    }
  }
  return true;
}

bool LCDHAL::BufferEmpty(void)
{
  return _Flags.BufferEmpty;
}

bool LCDHAL::BufferFull(void)
{
  return _Flags.BufferFull;
}

void LCDHAL:: Fastitoa(unsigned int InputInteger, unsigned int comma)
{
  char Digits[5];
  bool cero = false;
  bool ocultar = true;

  //TODO cap the number of digits printed
/*
  if(InputInteger>=30000)
  {
    if(InputInteger>=50000)
    {
      if(InputInteger>=60000)
      {
          InputInteger-=60000;
          Digits[0]=0x36;
      }
      else
      {
        InputInteger-=50000;
        Digits[0]=0x35;
      }
    }
    else
    {
      if(InputInteger>=40000)
      {
        InputInteger-=40000;
        Digits[0]=0x34;
      }
      else
      {
        InputInteger-=30000;
        Digits[0]=0x33;
      }
    }
  }
  else
  {
    if(InputInteger>=10000)
    {
      if(InputInteger>=20000)
      {
        InputInteger-=20000;
        Digits[0]=0x32;
      }
      else
      {
        InputInteger-=10000;
        Digits[0]=0x31;
      }
    }
    else
    {
      Digits[0]=0x30;
    }
  }
  */


  Digits[0]=0x20;


  if (comma == 0)
  {
    if(InputInteger>=5000)
    {
      if(InputInteger>=7000)
      {
        if(InputInteger>=8000)
        {
          if(InputInteger>=9000)
          {
            InputInteger-=9000;
            Digits[1]=0x39;
          }
          else
          {
            InputInteger-=8000;
            Digits[1]=0x38;
          }
        }
        else
        {
          InputInteger-=7000;
          Digits[1]=0x37;
        }
      }
      else
      {
        if(InputInteger>=6000)
        {
          InputInteger-=6000;
          Digits[1]=0x36;
        }
        else
        {
          InputInteger-=5000;
          Digits[1]=0x35;
        }
      }
    }
    else
    {
      if(InputInteger>=2000)
      {
        if(InputInteger>=3000)
        {
          if(InputInteger>=4000)
          {
            InputInteger-=4000;
            Digits[1]=0x34;
          }
          else
          {
            InputInteger-=3000;
            Digits[1]=0x33;
          }
        }
        else
        {
          InputInteger-=2000;
          Digits[1]=0x32;
        }
      }
      else
      {
        if(InputInteger>=1000)
        {
          InputInteger-=1000;
          Digits[1]=0x31;
        }
        else
        {
  //      Digits[1]=0x30;
          cero = true;
          if (ocultar) Digits[1]=0x20;
          else Digits[1]=0x30;
        }
      }
    }

    if (!cero) ocultar = false;
    cero = false;
  }

  if(InputInteger>=500)
  {
    if(InputInteger>=700)
    {
      if(InputInteger>=800)
      {
        if(InputInteger>=900)
        {
          InputInteger-=900;
          Digits[2]=0x39;
        }
        else
        {
          InputInteger-=800;
          Digits[2]=0x38;
        }
      }
      else
      {
        InputInteger-=700;
        Digits[2]=0x37;
      }
    }
    else
    {
      if(InputInteger>=600)
      {
        InputInteger-=600;
        Digits[2]=0x36;
      }
      else
      {
        InputInteger-=500;
        Digits[2]=0x35;
      }
    }
  }
  else
  {
    if(InputInteger>=200)
    {
      if(InputInteger>=300)
      {
        if(InputInteger>=400)
        {
          InputInteger-=400;
          Digits[2]=0x34;
        }
        else
        {
          InputInteger-=300;
          Digits[2]=0x33;
        }
      }
      else
      {
        InputInteger-=200;
        Digits[2]=0x32;
      }
    }
    else
    {
      if(InputInteger>=100)
      {
        InputInteger-=100;
        Digits[2]=0x31;
      }
      else
      {
//      Digits[2]=0x30;
        cero = true;
        if (ocultar) Digits[2]=0x20;
        else Digits[2]=0x30;
      }
    }
  }

  if (!cero) ocultar = false;
  cero = false;

  if ((comma == 1) || (comma == 2))
  {
    Digits[1] = Digits[2];
  }
  if (comma == 2) Digits[2] = 0x2E;


  if(InputInteger>=50)
  {
    if(InputInteger>=70)
    {
      if(InputInteger>=80)
      {
        if(InputInteger>=90)
        {
          InputInteger-=90;
          Digits[3]=0x39;
        }
        else
        {
          InputInteger-=80;
          Digits[3]=0x38;
        }
      }
      else
      {
        InputInteger-=70;
        Digits[3]=0x37;
      }
    }
    else
    {
      if(InputInteger>=60)
      {
        InputInteger-=60;
        Digits[3]=0x36;
      }
      else
      {
        InputInteger-=50;
        Digits[3]=0x35;
      }
    }
  }
  else
  {
    if(InputInteger>=20)
    {
      if(InputInteger>=30)
      {
        if(InputInteger>=40)
        {
          InputInteger-=40;
          Digits[3]=0x34;
        }
        else
        {
          InputInteger-=30;
          Digits[3]=0x33;
        }
      }
      else
      {
        InputInteger-=20;
        Digits[3]=0x32;
      }
    }
    else
    {
      if(InputInteger>=10)
      {
        InputInteger-=10;
        Digits[3]=0x31;
      }

      else
      {
//      Digits[3]=0x30;
        cero = true;
        if (ocultar) Digits[3]=0x20;
        else Digits[3]=0x30;
      }
    }
  }

  if (!cero) ocultar = false;
  cero = false;

  if (comma == 1)
  {
    Digits[2] = Digits[3];
    Digits[3] = 0x2E;
  }


  if(InputInteger>=5)
  {
    if(InputInteger>=7)
    {
      if(InputInteger>=8)
      {
        if(InputInteger>=9)
        {
          InputInteger-=9;
          Digits[4]=0x39;
        }
        else
        {
          InputInteger-=8;
          Digits[4]=0x38;
        }
      }
      else
      {
        InputInteger-=7;
        Digits[4]=0x37;
      }
    }
    else
    {
      if(InputInteger>=6)
      {
        InputInteger-=6;
        Digits[4]=0x36;
      }
      else
      {
        InputInteger-=5;
        Digits[4]=0x35;
      }
    }
  }
  else
  {
    if(InputInteger>=2)
    {
      if(InputInteger>=3)
      {
        if(InputInteger>=4)
        {
          InputInteger-=4;
          Digits[4]=0x34;
        }
        else
        {
          InputInteger-=3;
          Digits[4]=0x33;
        }
      }
      else
      {
        InputInteger-=2;
        Digits[4]=0x32;
      }
    }
    else
    {
      if(InputInteger>=1)
      {
        InputInteger-=1;
        Digits[4]=0x31;
      }
      else Digits[4]=0x30;
    }
  }
  Write(Digits,5);
}
