#ifndef LCDHAL_H
#define LCDHAL_H


#include <stdio.h>      // NECESARIO PARA LCDHAL



#define BufferSize 128

typedef struct
{
  unsigned char Data;
  unsigned Command:1;
  unsigned :7;
}sLCDBuffer;

typedef struct
{
  unsigned BufferFull:1;
  unsigned BufferEmpty:1;
  unsigned :6;
}sLCDFlags;

class LCDHAL
{
  public:
  LCDHAL(void);
  ~LCDHAL(void);
  bool BufferFull(void);
  bool BufferEmpty(void);
  bool Goto(unsigned char Column, unsigned char Row);
  bool Init(void);
  bool Clear();
  bool Write(const char *OutputString,unsigned char Size);
  bool Putchar(char acterChar);
  void Update(void);
  void Fastitoa(unsigned int InputInteger, unsigned int comma = 0);
  private:
  FILE _LCDOut={0};
  FILE _Oristdout;
  const /*PROGMEM*/ unsigned char _RowOffset[4]={0x80,0xC0,0x94,0xD4};
  sLCDFlags _Flags={false,true};
  sLCDBuffer _Buffer[BufferSize];
  bool _BufferPush(sLCDBuffer *LCDCommand);
  sLCDBuffer* _BufferPop(void);
  void _Print(sLCDBuffer *LCDData);
  unsigned char _BufferInputIndex=0,_BufferOutputIndex=0;
  static LCDHAL* _Instance;
  static int _Putchar(char c, FILE *stream);
  static int (*_oriPutchar)(char, FILE *);
};

#endif
