#include "Panel_LCD.h"
#include <Arduino.h>


  LCDHAL ReespiratorLCD;                                  // Crea el LCD con un objeto LCDHAL
  Teclado_panel teclado_panel (18);


Panel_LCD::Panel_LCD ()
{
  /*
   _BUTTONpin [1] = BUTTON1pin;
   _BUTTONpin [2] = BUTTON2pin;
   _BUTTONpin [3] = BUTTON3pin;
   _BUTTONpin [4] = BUTTON4pin;

   _encoderCLKpin = ecoderCLKPin;
   _encoderDIRpin = ecoderDIRPin;
   _encoderBOTpin = ecoderBOTPin;
   */

   init();

};



void Panel_LCD::init()                        // Inicializa el display. Carga los menús de variables para editar
{
    milisegundos_ant = millis();
    estado_display = State_Init;

    ReespiratorLCD.Init();                 // Inicializa el display
    ReespiratorLCD.Clear();

/*
    for (int i=1; i<5; i++)                   // Establece el modo de los pines asociados a los botones
    {
      pinMode (_BUTTONpin [1], INPUT);
    }

*/

  // Variables principales del sistema
      numParametros = 7;
      numConsignas  = 8;

            sprintf (medidas [0].etiqueta, " PEAK");
            medidas [0].valor_consigna = 28;
            medidas [0].valor_minimo =   9;
            medidas [0].valor_maximo =   35;
            medidas [0].valor_instantaneo = medidas [0].valor_consigna;
            medidas [0].alarma_minimo = medidas [0].valor_minimo;
            medidas [0].alarma_maximo = medidas [0].valor_maximo;
            sprintf (medidas [0].etiqueta_consigna,  " PEAK_consigna ");
            sprintf (medidas [0].etiqueta_alrm_bajo, "PeakPressureLow");
            sprintf (medidas [0].etiqueta_alrm_alto, "PeakPressureHig");

            sprintf (medidas [1].etiqueta, " PEEP");
            medidas [1].valor_consigna = 6;
            medidas [1].valor_minimo =   4;
            medidas [1].valor_maximo =   25;
            medidas [1].valor_instantaneo = medidas [1].valor_consigna;
            medidas [1].alarma_minimo = medidas [1].valor_minimo;
            medidas [1].alarma_maximo = medidas [1].valor_maximo;
            sprintf (medidas [1].etiqueta_consigna,  " PEEP_consigna ");
            sprintf (medidas [1].etiqueta_alrm_bajo, "PeepPressureLow");
            sprintf (medidas [1].etiqueta_alrm_alto, "PeepPressureHig");

            sprintf (medidas [2].etiqueta, "  RPM");
            medidas [2].valor_consigna = 15;
            medidas [2].valor_minimo =   5;
            medidas [2].valor_maximo =   30;
            medidas [2].valor_instantaneo = medidas [2].valor_consigna;
            medidas [2].alarma_minimo = medidas [2].valor_minimo;
            medidas [2].alarma_maximo = medidas [2].valor_maximo;
            sprintf (medidas [2].etiqueta_consigna,  " RPM_consigna  ");
            sprintf (medidas [2].etiqueta_alrm_bajo, "RPM_Low        ");
            sprintf (medidas [2].etiqueta_alrm_alto, "RPM_High       ");

            sprintf (medidas [3].etiqueta, "  VOL");
            medidas [3].valor_consigna = 400;
            medidas [3].valor_minimo =   100;
            medidas [3].valor_maximo =   900;
            medidas [3].valor_instantaneo = medidas [3].valor_consigna;
            medidas [3].alarma_minimo = medidas [3].valor_minimo;
            medidas [3].alarma_maximo = medidas [3].valor_maximo;
            sprintf (medidas [3].etiqueta_consigna,  " VOL_consigna ");
            sprintf (medidas [3].etiqueta_alrm_bajo, "VolumeLow     ");
            sprintf (medidas [3].etiqueta_alrm_alto, "VolumeHigh    ");

            sprintf (medidas [4].etiqueta, " t_on");
            medidas [4].valor_consigna = 0;
            medidas [4].valor_minimo =   0;
            medidas [4].valor_maximo =   1;
            medidas [4].valor_instantaneo = medidas [4].valor_consigna;;     // 0 => off; > 0 => on
            medidas [4].alarma_minimo = medidas [4].valor_minimo;
            medidas [4].alarma_maximo = medidas [4].valor_maximo;
            sprintf (medidas [4].etiqueta_consigna,  "TRIGGER on/off");

            sprintf (medidas [5].etiqueta, " trig");
            medidas [5].valor_consigna = 10;
            medidas [5].valor_minimo =   1;
            medidas [5].valor_maximo =   20;
            medidas [5].valor_instantaneo = medidas [5].valor_consigna;;     // 0 => off; > 0 => on
            medidas [5].alarma_minimo = medidas [5].valor_minimo;
            medidas [5].alarma_maximo = medidas [5].valor_maximo;
            sprintf (medidas [5].etiqueta_consigna,  "TRIGG_consigna");

            sprintf (medidas [6].etiqueta, "RECRU");
            medidas [6].valor_consigna = 0;       // tiempo recruit
            medidas [6].valor_minimo =   0;
            medidas [6].valor_maximo =   1;
            medidas [6].valor_instantaneo = medidas [6].valor_consigna;     // 0 => off; > 0 => on
            medidas [6].alarma_minimo = medidas [6].valor_minimo;
            medidas [6].alarma_maximo = medidas [6].valor_maximo;
            sprintf (medidas [6].etiqueta_consigna,  "RECRUIT on/off");

            sprintf (medidas [7].etiqueta, " IDLE");
            medidas [7].valor_consigna = 0;       // tiempo recruit
            medidas [7].valor_minimo =   0;
            medidas [7].valor_maximo =   1;
            medidas [7].valor_instantaneo = medidas [7].valor_consigna;;     // 0 => off; > 0 => on
            medidas [7].alarma_minimo = medidas [7].valor_minimo;
            medidas [7].alarma_maximo = medidas [7].valor_maximo;
            sprintf (medidas [7].etiqueta_consigna,  "PARAR_RESPIR  ");



// Rellena la linea de etiquetas
        for (int i = 0; i<5; i++)
        {
          linea_etiquetas[i]    = medidas [0].etiqueta[i];
          linea_etiquetas[i+ 5] = medidas [1].etiqueta[i];
          linea_etiquetas[i+10] = medidas [2].etiqueta[i];
          linea_etiquetas[i+15] = medidas [3].etiqueta[i];
        }

  sprintf (linea_alarmas, "(3) ALRM CIRCULANTE ");
}






void Panel_LCD::fin_de_inicio (void)                // Fin de inicio. Sirve para que el sistema avise del final del inicio
{
  if (estado_display == State_Init) estado_display = State_Idle;
}



int Panel_LCD::actualiza_linea_alarmas (int alarmas_activas)
{
  // Establece el valor de la variable char[] que se mostrara en la línea de alarmas
  static int alrm_indice = -1;
  int alrm_numero_activas = 0;
  int nuevo_indice = -1;

  for (int i=(alrm_indice+1); i<(alrm_indice+1)+16; i++)  // Recorre desde la alarma que se está mostrando
  {
    int ii = i % 16;
    if ( (alarmas_activas & (1<<ii)) )
    {
      if (nuevo_indice<0) nuevo_indice = ii;
      alrm_numero_activas ++;
    }
  }
  alrm_indice = nuevo_indice;

  // Refresca la linea de alarmas
  if (alrm_indice >= 0)
  {
    sprintf (linea_alarmas, "(%2i)               ", alrm_numero_activas);
    for (int i=0; i<15; i++)
    {
      linea_alarmas [i+5] = alarmas[alrm_indice][i];
    }
  }
  else
  {
    sprintf (linea_alarmas, "( 0)    - - - -     ");
  }

  linea_alarmas_actualizada = true;

  return alrm_numero_activas;
}


void Panel_LCD::update (int valor0, int valor1, int valor2, int valor3, unsigned int alarmas_activas)
{
  static int estado_display_ant;
  static unsigned long millis_refresco_pantalla, milisegundos_fin_edicion;
  static bool cambio_estado = true;

  static int consigna_nueva_ant;
  static int consigna_ant, consigna_nueva;
  static int edicion_linea = 0;
  static int valor_edita_ant;
  static bool edicion_parametro = false;
  bool pulsa_boton;
  static unsigned long milisegundos_refresco_alarmas;
  static unsigned long milis_entrada_iddle;
  static unsigned long milis_fin_recruit;

//  int consigna_nueva;
  static int minimo_consigna;
  static int maximo_consigna;
  int valor_encoder;
  int valor_edita;
  char etiqueta [16];
  int boton_seleccionado;
  bool refresca_pantalla = false;

  milisegundos = millis();


  cambio_estado = estado_display != estado_display_ant;    // Determina su en la última iteración cambió el estado del display
  estado_display_ant = estado_display;

  if (cambio_estado)                                       // Fuerza la actualización de la pantalla cuando hay cambio de estado
  {
    linea_consignas_actualizada = true;
    linea_valores_actualizada = true;
    linea_alarmas_actualizada = true;

    refresca_pantalla = true;
  }

  if (milisegundos > millis_refresco_pantalla)             // Fuerza la actualización de la pantalla cada cierto tiempo
  {
    millis_refresco_pantalla = milisegundos + intervaloRefresco;
    refresca_pantalla = true;
  }

  if (refresca_pantalla)                                  // Si refresca pantalla toma los últimos valores
  {                                                       // Solo detecta los cambios de valor cuando va a renovar la pantalla.
    medidas [0].valor_instantaneo = valor0;
    medidas [1].valor_instantaneo = valor1;
    medidas [2].valor_instantaneo = valor2;
    medidas [3].valor_instantaneo = valor3;


    for (int i=0; i<5; i++)
    {
      if (medidas [i].valor_instantaneo_anterior != medidas [i].valor_instantaneo) linea_valores_actualizada = true;

      medidas [i].valor_instantaneo_anterior = medidas [i].valor_instantaneo;
    }
  }


  // Actualiza los perifericos
  ReespiratorLCD.Update();                                  // actualiza el buffer del display
  boton_seleccionado = teclado_panel.update_teclas();       // muestrea el teclado
  valor_encoder = teclado_panel.update_encoder();                           // muestrea el encoder
  pulsa_boton = teclado_panel.update_tecla_encoder();

  if (milisegundos > milisegundos_refresco_alarmas)
  {
    milisegundos_refresco_alarmas = milisegundos + tiempo_refresco_alarmas;
    actualiza_linea_alarmas (alarmas_activas);
  }


   switch (estado_display) {                                                // Solo se consideran estados 10:normal y 11: edición
        /*************************************************************
         *          STATE_INIT                                       *
         *************************************************************/
        case State_Init:                // equipo iniciando
        {
          // Actualiza la pantalla
                if (refresca_pantalla) Pantalla_State_Init (cambio_estado);                              // Dibuja la pantalla "normal"
        }
        break;


        /*************************************************************
         *          STATE_IDLE                                       *
         *************************************************************/
        case  State_Idle:                // configuración parado
        {

              if (cambio_estado)         // Entrada en el estado
              {
                parametro_conf = 0;
                consigna_nueva = 0;
                consigna_nueva_ant = 0;
                edicion_linea = 0;

                milis_entrada_iddle = milisegundos;
              }



            // Transiciones asociadas con los botones
                if ((boton_seleccionado == 1)|| (boton_seleccionado == 2))                   // Cambia al estado de edicion si se pulsa un botón
                {
                  estado_display = State_Conf_Idle;
                }

                else if ((boton_seleccionado == 3)|| (boton_seleccionado == 4))              // RSe pone en marcha si se pulsa un botón
                {
                  estado_display = State_Working_PC;
                }

            // Actualiza la pantalla
                if (refresca_pantalla)
                {
                  int segs_iddle = (milisegundos - milis_entrada_iddle)/1000;
                  Pantalla_State_Idle (cambio_estado, segs_iddle);                  // Dibuja la pantalla configuración parado
                }
        }
        break;


        /*************************************************************
         *          STATE_CONFIGURACION_EN_IDLE                      *
         *************************************************************/
        case  State_Conf_Idle:                // configuración parametro parado
        {

            // Cálculos asociados con los botones
                if (boton_seleccionado == 1)                                                 // configura el valor de consigna
                {
                  valor_edita = medidas [parametro_conf].valor_consigna;
                  teclado_panel.set_encoder (valor_edita);
                  edicion_linea = 1;
                }

               else if (boton_seleccionado == 2)                                                  // configura el valor de alarma por mínimo
                {
                  valor_edita = medidas [parametro_conf].alarma_minimo;
                  teclado_panel.set_encoder (valor_edita);
                  edicion_linea = 2;
                }

                else if (boton_seleccionado == 3)                                                 //configura el valor de alarma por maximo
                {
                  valor_edita = medidas [parametro_conf].alarma_maximo;
                  teclado_panel.set_encoder (valor_edita);
                  edicion_linea = 3;
                }

                else if ((boton_seleccionado == 4) && (edicion_linea != 0))        // Cambia al estado de edicion si se pulsa un botón
                {
                  valor_edita = 0;
                  teclado_panel.set_encoder (valor_edita);
                  edicion_linea = 0;
                }

            // Cálculos asociados con el encoder
                if (refresca_pantalla)
                {
                  if (edicion_linea == 0)                                 // Seleccion del parámetro
                  {
                    if (valor_encoder > 0) parametro_conf ++;
                    if (valor_encoder < 0) parametro_conf --;

                    if (parametro_conf <  0) parametro_conf = numParametros -1;
                    if (parametro_conf >= numParametros) parametro_conf =  0;

                    valor_encoder = 0;
                    teclado_panel.set_encoder (valor_encoder);
                  }

                  else if (edicion_linea == 1)                              // edicion del valor
                  {
                    valor_edita = valor_encoder;                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (valor_edita != valor_edita_ant)
                    {
                      valor_edita = constrain (valor_edita,
                                                    medidas [parametro_conf].valor_minimo,
                                                    medidas [parametro_conf].valor_maximo);
                      teclado_panel.set_encoder (valor_edita);
                      medidas [parametro_conf].valor_consigna = valor_edita;
                      valor_edita_ant = valor_edita;
                    }
                  }

                  else if (edicion_linea == 2)
                  {
                    valor_edita = valor_encoder;                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (valor_edita != valor_edita_ant)
                    {
                      valor_edita = constrain (valor_edita,
                                                    medidas [parametro_conf].valor_minimo,
                                                    medidas [parametro_conf].valor_maximo);
                      teclado_panel.set_encoder (valor_edita);
                      medidas [parametro_conf].alarma_minimo = valor_edita;
                      valor_edita_ant = valor_edita;
                    }
                  }

                  else if (edicion_linea == 3)
                  {
                    valor_edita = valor_encoder;                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (valor_edita != valor_edita_ant)
                    {
                      valor_edita = constrain (valor_edita,
                                                    medidas [parametro_conf].valor_minimo,
                                                    medidas [parametro_conf].valor_maximo);
                      teclado_panel.set_encoder (valor_edita);
                      medidas [parametro_conf].alarma_maximo = valor_edita;
                      valor_edita_ant = valor_edita;
                    }
                  }
                }

            // Transiciones asociadas con los botones
                if ((boton_seleccionado == 4) && (edicion_linea == 0)) estado_display = State_Idle;        // Va a estado State_Idle

            // Actualiza la pantalla
                if (refresca_pantalla)                              // Dibuja la pantalla configuración parado
                   Pantalla_State_Conf_Idle (cambio_estado,
                                             parametro_conf,
                                             edicion_linea,
                                             valor_edita);
        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC                                 *
         *************************************************************/
        case  State_Working_PC:                // estado normal
        {
//          if (cambio_estado) {};

            // Cálculos asociados con los botones
                ultimo_boton_seleccionado = 0;
                if (boton_seleccionado != 0)                                    // Cambia al estado de edicion si se pulsa un botón
                {
                  if (boton_seleccionado <= 3)
                  {

                    boton_pulsado_seleccionado = boton_seleccionado;
                    milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;
                    ultimo_boton_seleccionado = boton_seleccionado;
                    consigna_ant = medidas[ultimo_boton_seleccionado-1].valor_consigna;
                    teclado_panel.set_encoder (consigna_ant);

                    estado_display = State_Working_PC_edit_1;
                  }
                  else if (boton_seleccionado == 4)
                  {
                    estado_display = State_Working_PC_edit_2;
                  }
                }


            // Actualiza la pantalla
               if (refresca_pantalla) Pantalla_State_Working_PC (cambio_estado);                // Dibuja la pantalla "normal"
        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC_EDIT_BASICO                     *
         *************************************************************/
        case  State_Working_PC_edit_1:                // edicion parámetro ajuste
        {
//          if (cambio_estado) milisegundos_sin_interm = 0;

            // Cálculos asociados con los botones
                if (boton_seleccionado == boton_pulsado_seleccionado)                        // Si se pulsa el mismo botón extiende el tiempo de edición
                            milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;

            // Cálculos asociados con el encoder
                  consigna_nueva = valor_encoder;                                       // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                  if (consigna_nueva != consigna_nueva_ant)
                  {
                    consigna_nueva = constrain (consigna_nueva,
                                        medidas [ultimo_boton_seleccionado-1].valor_minimo,
                                        medidas [ultimo_boton_seleccionado-1].valor_maximo);
                    teclado_panel.set_encoder (consigna_nueva);
                    consigna_nueva_ant = consigna_nueva;

                    milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;         // extiende el tiempo de edcición
                    milisegundos_sin_interm = milisegundos + 1000;
                  }

            // Cálculos asociados al botón del encoder
                  if (pulsa_boton)
                  {
                    medidas [ultimo_boton_seleccionado-1].valor_consigna = consigna_nueva;
                    milisegundos_fin_edicion = milisegundos + 4000;
                    milisegundos_sin_interm = milisegundos + 5000;
                    estado_display = State_Working_PC_edit_1_conf;
                  }

            // Transiciones asociadas con los botones
                  if ((boton_seleccionado != boton_pulsado_seleccionado) &&                  // Si se pulsa otro botón vuelve al estado normal
                      (boton_seleccionado != 0))
                              estado_display = State_Working_PC;

            // Transiciones asociadas al tiempo
                  else if (milisegundos > milisegundos_reposo_edicion)                      // Si pasa el tiempo máximo de edición vuelve al menú normal
                                estado_display = State_Working_PC;

            // Actualiza la pantalla
                   if (refresca_pantalla)                                                   // Dibuja la pantalla "edicion"
                        Pantalla_State_Working_PC_edit_1 (cambio_estado, consigna_ant, consigna_nueva);
        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_EDIT_1 (BASICO)        *
         *************************************************************/
        case State_Working_PC_edit_1_conf:                // fin edicion parámetro ajuste
        {
          if (cambio_estado) {}

            // Cálculos asociados con los botones

            // Cálculos asociados con el encoder

            // Cálculos asociados al botón del encoder

            // Transiciones asociadas con los botones
                  if ((boton_seleccionado != boton_pulsado_seleccionado) &&                               // Si se pulsa otro botón vuelve al estado normal
                      (boton_seleccionado != 0)) estado_display = State_Working_PC;

            // Transiciones asociadas al tiempo
                  if (milisegundos > milisegundos_fin_edicion)  estado_display = State_Working_PC;        // Si pasa el tiempo máximo de edición vuelve al menú normal

            // Actualiza la pantalla
                   if (refresca_pantalla)                                                                  // Dibuja la pantalla "edicion"
                          Pantalla_State_Working_PC_edit_1 (cambio_estado, consigna_ant, consigna_nueva);
        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_EDIT_2 (AVANZADO)      *
         *************************************************************/
        case  State_Working_PC_edit_2:                // fin edicion parámetro ajuste
        {
          int parametro_ant = parametro_conf;
          consigna_nueva_ant = consigna_nueva;

          if (cambio_estado)
          {
            teclado_panel.set_encoder (0);
            edicion_parametro = false;                                            // modo de selección de valor a editar
            parametro_conf = 0;
                parametro_ant = -1;                                               // fuerza carga de valores del parámetro
            milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;      // Tiempo de edicion
            milisegundos_sin_interm = 0;                                          // Parpadea desde el pricipio
          }


          // Cálculos asociados con el encoder
                if (!edicion_parametro)                                            // Modo de selección de parámetro
                {
                      if (refresca_pantalla)                                       // Solo actualiza el cambio de parámetro cuando actualiza pantalla
                      {
                        if (valor_encoder != 0)
                        {
                          if (valor_encoder > 0)
                          {
                            parametro_conf ++;
                            if ((parametro_conf == 13) || (parametro_conf == 14)) parametro_conf = 15;  // activación trigger
                            if ((parametro_conf == 16) || (parametro_conf == 17)) parametro_conf = 18;  // valor trigger
                            if ((parametro_conf == 19) || (parametro_conf == 20)) parametro_conf = 21;  // valor RECRUIT
                            if ((parametro_conf == 22) || (parametro_conf == 23)) parametro_conf = 21;  // Ir a IDDLE
                          }
                          else if (valor_encoder < 0)
                          {
                            parametro_conf --;
                            if ((parametro_conf == 13) || (parametro_conf == 14)) parametro_conf = 12;  // activación trigger
                            if ((parametro_conf == 16) || (parametro_conf == 17)) parametro_conf = 15;  // valor trigger
                            if ((parametro_conf == 19) || (parametro_conf == 20)) parametro_conf = 18;  // valor RECRUIT
                            if ((parametro_conf == 22) || (parametro_conf == 23)) parametro_conf = 21;  // Ir a IDDLE
                          }
                          teclado_panel.set_encoder (0);
                          milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;    // Extiende tiempo edición
                          milisegundos_sin_interm = milisegundos + 1000;
                        }

                        if (parametro_conf != parametro_ant)
                        {
                          parametro_conf = constrain (parametro_conf, 0, numParametros*3);  ////*******
                          int grupo_param_conf = parametro_conf/3;

                          if (parametro_conf % 3 == 0)
                          {
                            consigna_nueva  = medidas[grupo_param_conf].valor_consigna;
                            minimo_consigna = medidas[grupo_param_conf].valor_minimo;
                            maximo_consigna = medidas[grupo_param_conf].valor_maximo;
                            for (int i=0; i<16; i++) {etiqueta [i] = medidas[grupo_param_conf].etiqueta_consigna[i];}
                          }
                          else if (parametro_conf % 3 == 1)
                          {
                            consigna_nueva  = medidas[grupo_param_conf].alarma_minimo;
                            minimo_consigna = medidas[grupo_param_conf].valor_minimo;
                            maximo_consigna = medidas[grupo_param_conf].valor_maximo;
                            for (int i=0; i<16; i++) {etiqueta [i] = medidas[grupo_param_conf].etiqueta_alrm_bajo[i];}
                          }
                          else
                          {
                            consigna_nueva  = medidas[grupo_param_conf].alarma_maximo;
                            minimo_consigna = medidas[grupo_param_conf].valor_minimo;
                            maximo_consigna = medidas[grupo_param_conf].valor_maximo;
                            for (int i=0; i<16; i++) {etiqueta [i] = medidas[grupo_param_conf].etiqueta_alrm_alto[i];}
                          }
                        }
                      }
                }
                else                                                                // Modo de edicion de parámetro
                {
                      consigna_nueva = valor_encoder;                           // Si se modificó el encoder muestra el valor
                      if (consigna_nueva != consigna_nueva_ant)
                      {
                        consigna_nueva = constrain (consigna_nueva, minimo_consigna, maximo_consigna);
                        teclado_panel.set_encoder (consigna_nueva);

                        milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;           // extiende el tiempo de edcición
                        milisegundos_sin_interm = milisegundos + 1000;
                      }
                 }

            // Cálculos asociados al botón del encoder
                if (pulsa_boton)
                {
                  if (!edicion_parametro)                                          // pasa a modo edición
                  {
                    edicion_parametro = true ;
                    teclado_panel.set_encoder (consigna_nueva);
                  }
                  else                                                            // graba el nuevo valor y pasa a modo seleccion
                  {
                    int grupo_param_conf = parametro_conf/3;
                    if (parametro_conf % 3 == 0)
                      medidas[grupo_param_conf].valor_consigna = consigna_nueva;
                    else if (parametro_conf % 3 == 1)
                      medidas[grupo_param_conf].alarma_minimo  = consigna_nueva;
                    else
                      medidas[grupo_param_conf].alarma_maximo  = consigna_nueva;

                    edicion_parametro = false ;
                    teclado_panel.set_encoder (0);
                  }
                  milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;
                  milisegundos_sin_interm = milisegundos + 1000;
                }

            // Transiciones asociadas con los botones
                if (boton_seleccionado != 0) estado_display = State_Working_PC;    // Si se pulsa algún botón vuelve al estado normal

            // Transiciones asociadas al tiempo
                else if (milisegundos > milisegundos_reposo_edicion)  estado_display = State_Working_PC; // Si pasa el tiempo máximo de edición vuelve al menú normal

            // Transiciones asociadas al botón del encoder
                  // Si se consigna valor RECRUIT cambia al estado State_RECRUIT:
                  else if (medidas[6].valor_consigna == 1)
                          estado_display = State_RECRUIT;

            // Si se consigna valor on en GOTO_IDLE cambia al estado State_Confirm_gotoIdle:
                  else if (medidas[7].valor_consigna == 1)
                      estado_display = State_Confirm_gotoIdle;

            // Actualiza la pantalla
                  if (refresca_pantalla)
                          Pantalla_State_Working_PC_edit_2 (cambio_estado,
                                                            edicion_parametro,
                                                            consigna_nueva,
                                                            parametro_conf,
                                                            etiqueta);                             // Actualiza la pantalla "edicion
        }
        break;





        /*************************************************************
         *          STATE_RECRUIT                                    *
         *************************************************************/
        case State_RECRUIT:                // Modo RECRUIT
        {
          if (cambio_estado)         // Entrada en el estado
          {
            milis_fin_recruit = milisegundos + 40000;
          }

            // Transiciones asociadas con los botones
                if (boton_seleccionado > 0)
                {
                  medidas[6].valor_consigna = 0;
                  estado_display = State_Working_PC;
                }
            // Transiciones asociadas al tiempo
                else if (milisegundos > milis_fin_recruit)
                {
                  medidas[6].valor_consigna = 0;
                  estado_display = State_Working_PC;
                }
                // Actualiza la pantalla
                int seg_resta = (milis_fin_recruit - milisegundos) / 1000;

                if (refresca_pantalla) Pantalla_State_RECRUIT (cambio_estado, seg_resta);

        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_GO_TO_IDLE             *
         *************************************************************/
        case State_Confirm_gotoIdle:                // fin edicion parámetro ajuste
        {
          if (cambio_estado) milisegundos_fin_edicion = milisegundos + tiempo_max_edicion;     // Tiempo de edicion

            // Transiciones asociadas con los botones
                if ((boton_seleccionado > 0) & (boton_seleccionado != 3)) estado_display = State_Working_PC;          // Si se pulsa algún botón vuelve al estado normal
                else if (boton_seleccionado == 3) estado_display = State_Idle;

            // Transiciones asociadas al tiempo
                else if (milisegundos > milisegundos_fin_edicion)  estado_display = State_Working_PC;                // Si pasa el tiempo máximo de edición vuelve al menú normal
                medidas[7].valor_consigna = 1;
            // Actualiza la pantalla
                  if (refresca_pantalla) Pantalla_State_Confirm_gotoIdle (cambio_estado);
        }
        break;


        default:
        {
            estado_display = State_Idle;
        }
        break;
    };


  linea_consignas_actualizada = false;
  linea_valores_actualizada = false;
//   linea_alarmas_actualizada = false;


}


/**********************************************************************
 *          PANTALLA_STATE_INIT                                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Init (bool cambio)
{
  static byte contador = 0;

  if (cambio && (contador!=0)) contador = 0;

  if (contador  == 1)
  {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write("  REESPIRATOR 2020  ", 20);
  }

  if (contador  == 8)
  {
      ReespiratorLCD.Goto(0,1);
      ReespiratorLCD.Write("de personas         ", 20);
  }

  if (contador  == 12)
  {
      ReespiratorLCD.Goto(0,2);
      ReespiratorLCD.Write("       para personas", 20);
  }

  if (contador  == 20)
  {
      ReespiratorLCD.Goto(0,3);
      ReespiratorLCD.Write(" v XXX.YYY          ", 20);
  }

if (contador  < 30) contador ++;

}


/**********************************************************************
 *          PANTALLA_STATE_IDLE                                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Idle (bool cambio, int segs_iddle)
{
  static byte contador = 0;
  static int segs_ant;
  if (cambio && (contador!=0)) contador = 0;

  if (contador  == 1) ReespiratorLCD.Clear();

  if (contador  == 3)
  {
        ReespiratorLCD.Goto(2,0);
        ReespiratorLCD.Write("REPOSO", 6);
  }

  if (contador  == 4)
  {
        ReespiratorLCD.Goto(0,2);
        ReespiratorLCD.Write(" MODO PRESS CONTROL ", 20);
  }

  if (contador  == 5)
  {
        ReespiratorLCD.Goto(0,3);
        ReespiratorLCD.Write("  config   modo  on ", 20);
  }

  else if ((contador  == 3) || (segs_iddle != segs_ant))
  {
    char lineaEdicion[21];
    sprintf (lineaEdicion, "%3i:%2i:%2i",
                      segs_iddle/3600,
                      (segs_iddle / 60) % 60,
                      segs_iddle % 60);
    ReespiratorLCD.Goto(9,0);
    ReespiratorLCD.Write(lineaEdicion, 9);
  }

  if (contador  < 12) contador ++;

  segs_ant = segs_iddle;
}


/**********************************************************************
 *          PANTALLA_STATE_CONFIGURAR_EN_REPOSO                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Conf_Idle (bool cambio, int parametro ,int linea, int valor)
{
  static bool interm_ant;
  static int parametro_ant;
  static int linea_ant;
  static int valor_ant;

  bool interm = ((milisegundos/500) % 2 == 0);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)

  static byte contador = 0;
  if (cambio && (contador!=0)) contador = 0;

  if (parametro != parametro_ant) contador = 2;

  if (contador  == 1) ReespiratorLCD.Clear();

  else if (contador  == 2)
  {
    ReespiratorLCD.Goto(0,0);
    ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

    ReespiratorLCD.Goto(5,0);
    ReespiratorLCD.Write(" consigna=", 10);

    ReespiratorLCD.Goto(15,0);
    ReespiratorLCD.Fastitoa(medidas[parametro].valor_consigna);
  }

  else if (contador  == 3)
  {
    ReespiratorLCD.Goto(0,1);
    ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

    ReespiratorLCD.Goto(5,1);
    ReespiratorLCD.Write(" ALR_min = ", 10);

    ReespiratorLCD.Goto(15,1);
    ReespiratorLCD.Fastitoa(medidas[parametro].alarma_minimo);
  }

  else if (contador  == 4)
  {
    ReespiratorLCD.Goto(0,2);
    ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

    ReespiratorLCD.Goto(5,2);
    ReespiratorLCD.Write(" ALR_max = ", 10);

    ReespiratorLCD.Goto(15,2);
    ReespiratorLCD.Fastitoa(medidas[parametro].alarma_maximo);
  }

  else if (contador  == 5)
  {
    ReespiratorLCD.Goto(0,3);
    ReespiratorLCD.Write("cons  MIN  MAX  <<< ", 20);
  }

  else if (contador > 5)
  {
    if (valor != valor_ant)
    {
      ReespiratorLCD.Goto(15,linea-1);
      ReespiratorLCD.Fastitoa(valor);
    }
    if (linea != linea_ant)
    {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

      ReespiratorLCD.Goto(0,1);
      ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

      ReespiratorLCD.Goto(0,2);
      ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);

      ReespiratorLCD.Goto(15,3);
      if (linea == 0) ReespiratorLCD.Write(" <<< ", 5);
      else ReespiratorLCD.Write("  ok ", 5);
    }

    else if (linea != 0)
    {
      if (interm != interm_ant)
      {
        if (interm)
        {
          ReespiratorLCD.Goto(0,linea-1);
          ReespiratorLCD.Write(medidas[parametro].etiqueta, 5);
        }
        else
        {
          ReespiratorLCD.Goto(0,linea-1);
          ReespiratorLCD.Write("     ", 5);
        }
      }
    }
  }

  if (contador  < 12) contador ++;

  interm_ant = interm;
  linea_ant = linea;
  valor_ant = valor;
  parametro_ant = parametro;
}


/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC                                 *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC (bool cambio)
{
  static byte contador = 0;
  static bool interm_ant;
  static bool interm2_ant;
  bool interm = ((milisegundos/500) % 10 == 1);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)
  bool interm2 = ((milisegundos/500) % 10 < 5);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)


  if (cambio && (contador!=0)) contador = 0;


  if (contador  == 1) ReespiratorLCD.Clear();

  if (contador  == 2)
  {
    ReespiratorLCD.Goto(0,0);
    ReespiratorLCD.Write(linea_alarmas,20);
  }

  else if (contador  == 3)
  {
    ReespiratorLCD.Goto(0,1);
    ReespiratorLCD.Fastitoa(medidas[0].valor_consigna);
    ReespiratorLCD.Goto(0,2);
    ReespiratorLCD.Fastitoa(medidas[0].valor_instantaneo);
    ReespiratorLCD.Goto(0,3);
    ReespiratorLCD.Write(medidas[0].etiqueta, 5);
  }

  else if (contador  == 4)
  {
    ReespiratorLCD.Goto(5,1);
    ReespiratorLCD.Fastitoa(medidas[1].valor_consigna);
    ReespiratorLCD.Goto(5,2);
    ReespiratorLCD.Fastitoa(medidas[1].valor_instantaneo);
    ReespiratorLCD.Goto(5,3);
    ReespiratorLCD.Write(medidas[1].etiqueta, 5);
  }

  else if (contador  == 5)
  {
    ReespiratorLCD.Goto(10,1);
    ReespiratorLCD.Fastitoa(medidas[2].valor_consigna);
    ReespiratorLCD.Goto(10,2);
    ReespiratorLCD.Fastitoa(medidas[2].valor_instantaneo);
    ReespiratorLCD.Goto(10,3);
    ReespiratorLCD.Write(medidas[2].etiqueta, 5);
  }

  else if (contador  == 6)
  {
    ReespiratorLCD.Goto(15,1);
    ReespiratorLCD.Write(" trig", 5);
    //ReespiratorLCD.Fastitoa(medidas[3].valor_consigna);
    ReespiratorLCD.Goto(15,2);
    ReespiratorLCD.Fastitoa(medidas[3].valor_instantaneo);
    ReespiratorLCD.Goto(15,3);
    ReespiratorLCD.Write(medidas[3].etiqueta, 5);
  }


  else if (contador > 6)
  {
    if (linea_alarmas_actualizada)              // Actualiza alarmas
    {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write(linea_alarmas,20);

      linea_alarmas_actualizada = false;
    }

    if (linea_valores_actualizada)              // Actualiza valores
    {
      for (int i=0; i<4; i++)
      {
        ReespiratorLCD.Goto(5*i,2);
        ReespiratorLCD.Fastitoa(medidas[i].valor_instantaneo);
      }

      linea_valores_actualizada = false;
    }

    if (interm2 & !interm2_ant)                 // intermitencia trigger
    {
      ReespiratorLCD.Goto(15,1);
      ReespiratorLCD.Write(" trig",5);
    }
    else if (!interm2 & interm2_ant)
    {
       ReespiratorLCD.Goto(15,1);
       ReespiratorLCD.Write("  1:2",5);
    }

    if (interm & !interm_ant)                   // intermitencia conf
    {
      ReespiratorLCD.Goto(15,3);
      ReespiratorLCD.Write(" conf",5);
    }
    else if (!interm & interm_ant)
    {
       ReespiratorLCD.Goto(15,3);
       ReespiratorLCD.Write(medidas [3].etiqueta,5);
    }
  }

  if (contador  < 12) contador ++;

  interm_ant = interm;
  interm2_ant = interm2;
}



/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC_EDIT_1 (BASICO)                 *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC_edit_1 (bool cambio, int consigna_orig, int consigna_nueva)
{
  static byte contador = 0;
  static bool interm_ant;
  static int consigna_nueva_ant;
  bool interm = ((milisegundos/500) % 2 == 0);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)

  if (cambio && (contador!=0)) contador = 0;

  else if (contador  == 1)                              // Escribe la linea de edicion al entrar
  {
    char lineaEdicion[21];
    sprintf (lineaEdicion, "(%4i) => %4i ", consigna_orig, consigna_nueva);
    ReespiratorLCD.Goto(0,1);
    ReespiratorLCD.Write(medidas [boton_pulsado_seleccionado-1].etiqueta,5);
    ReespiratorLCD.Goto(5,1);
    ReespiratorLCD.Write(lineaEdicion,15);
  }

  else if (contador > 1)
  {
    if (linea_alarmas_actualizada)                       // Actualiza alarmas
    {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write(linea_alarmas,20);

      linea_alarmas_actualizada = false;
    }

    if (linea_valores_actualizada)                       // Actualiza valores
    {
      for (int i=0; i<4; i++)
      {
        ReespiratorLCD.Goto(5*i,2);
        ReespiratorLCD.Fastitoa(medidas[i].valor_instantaneo);
      }

      linea_valores_actualizada = false;
    }

    if (consigna_nueva_ant != consigna_nueva)            // refleja los cambios en la consigna editada
    {
      ReespiratorLCD.Goto(14,1);
      ReespiratorLCD.Fastitoa(consigna_nueva);
    }

    if ((interm && !interm_ant))                        // Gestiona las intermitencias
    {
      char lineaEdicion[21];
      sprintf (lineaEdicion, "(%4i) => %4i ", consigna_orig, consigna_nueva);
      ReespiratorLCD.Goto(0,1);
      ReespiratorLCD.Write(medidas [boton_pulsado_seleccionado-1].etiqueta,5);
      ReespiratorLCD.Goto(5,1);
      ReespiratorLCD.Write(lineaEdicion,15);

      ReespiratorLCD.Goto(5* (boton_pulsado_seleccionado-1), 3);
      ReespiratorLCD.Write("     ", 5);
    }

    else if (!interm && interm_ant)
    {
      ReespiratorLCD.Goto(5* (boton_pulsado_seleccionado-1), 3);
      ReespiratorLCD.Write(medidas [boton_pulsado_seleccionado-1].etiqueta,5);

      if (milisegundos > milisegundos_sin_interm)
      {
        ReespiratorLCD.Goto(0, 1);
        ReespiratorLCD.Write("                    ", 20);
      }
    }
  }

  if (contador  < 3) contador ++;

  consigna_nueva_ant = consigna_nueva;
  interm_ant = interm;
};



/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC_EDIT_2 (AVANZADO)               *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC_edit_2 (bool cambio,
                                                  bool edicion,
                                                  int consigna,
                                                  int parametro,
                                                  char etiqueta[16])
{
  static byte contador = 0;
  static bool interm_ant;
  static int consigna_ant;
  static int parametro_ant;
  bool interm = ((milisegundos/500) % 2 == 0);          //  Necesario para parpadear la etiqueta


  if (cambio && (contador!=0)) contador = 0;

  else if (contador  == 1)                              // Escribe la linea de edicion al entrar
  {
    ReespiratorLCD.Goto(0,1);
    ReespiratorLCD.Write(etiqueta,13);
    ReespiratorLCD.Goto(13,1);
    ReespiratorLCD.Write(" =",3);
    ReespiratorLCD.Goto(15,1);
      if ((parametro == 12 ) || (parametro == 18) || (parametro == 21))     // Caso especial activación de trigger y paso a IDLE
      {
        if (consigna == 0) ReespiratorLCD.Write(" off ",5);
        else  ReespiratorLCD.Write("  on ",5);
      }
      else ReespiratorLCD.Fastitoa(consigna);
  }

  else if (contador > 1)
  {
    if (linea_alarmas_actualizada)                       // Actualiza alarmas
    {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write(linea_alarmas,20);

      linea_alarmas_actualizada = false;
    }

    if (linea_valores_actualizada)                       // Actualiza valores
    {
      for (int i=0; i<4; i++)
      {
        ReespiratorLCD.Goto(5*i,2);
        ReespiratorLCD.Fastitoa(medidas[i].valor_instantaneo);
      }

      linea_valores_actualizada = false;
    }

    if (parametro != parametro_ant)                      // refleja los cambios en la consigna editada
    {
      ReespiratorLCD.Goto(0,1);
      ReespiratorLCD.Write(etiqueta,13);
    }

    if ((interm && !interm_ant) || (consigna != consigna_ant) || (parametro != parametro_ant))
    {
      ReespiratorLCD.Goto(15,1);
      if ((parametro == 12 ) || (parametro == 18) || (parametro == 21))     // Caso especial activación de trigger y paso a IDLE
      {
        if (consigna == 0) ReespiratorLCD.Write(" off ",5);
        else  ReespiratorLCD.Write("  on ",5);
      }
      else ReespiratorLCD.Fastitoa(consigna);
    }

    if (interm && !interm_ant)                        // Gestiona las intermitencias
    {
      /*
        ReespiratorLCD.Goto(15,1);
        if ((parametro == 12 ) || (parametro == 21))     // Caso especial activación de trigger y paso a IDLE
        {
          if (consigna == 0) ReespiratorLCD.Write(" off ",5);
          else  ReespiratorLCD.Write("  on ",5);
        }
        else ReespiratorLCD.Fastitoa(consigna);
      */

        ReespiratorLCD.Goto(0,1);
        ReespiratorLCD.Write(etiqueta,13);

    }

    else if (!interm && interm_ant)
    {
      if (edicion)              // Gestiona la intermitencia en edición del parámetro
      {
        if (milisegundos > milisegundos_sin_interm)
        {
          ReespiratorLCD.Goto(15,1);
          ReespiratorLCD.Write("     ",5);
        }
      }
      else
      {
        ReespiratorLCD.Goto(0,1);
        ReespiratorLCD.Write("             ",13);
      }
    }
  }

  if (contador  < 3) contador ++;

  consigna_ant = consigna;
  interm_ant  = interm;
  parametro_ant = parametro;
};


/**********************************************************************
 *          PANTALLA_STATE_RECRUIT                                    *
 **********************************************************************/
void Panel_LCD::Pantalla_State_RECRUIT (bool cambio, int seg_resta)
{
  static byte contador = 0;
  static int segs_ant;
  if (cambio && (contador!=0)) contador = 0;

  if (contador  == 1) ReespiratorLCD.Clear();

  if (contador  == 2)
  {
        ReespiratorLCD.Goto(1,0);
        ReespiratorLCD.Write("MODO RECRUIT", 12);
  }

  if (contador  == 3)
  {
        ReespiratorLCD.Goto(2,1);
        ReespiratorLCD.Write("restan: ", 8);
        ReespiratorLCD.Goto(14,1);
        ReespiratorLCD.Write(" seg", 4);
  }

  if (contador  == 5)
  {
        ReespiratorLCD.Goto(1,3);
        ReespiratorLCD.Write(" pulsar para salir  ", 20);
  }

  else if ((contador  == 4) || (seg_resta != segs_ant))
  {
    ReespiratorLCD.Goto(9,1);
    ReespiratorLCD.Fastitoa(seg_resta);
  }

  if (contador  < 10) contador ++;

  segs_ant = seg_resta;
}



/**********************************************************************
 *          PANTALLA_STATE_CONFIRM_GOTOIDLE                           *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Confirm_gotoIdle (bool cambio)
{
  bool intermitencia = ((milisegundos/500) % 2 == 0);          //  Necesario para parpadear la etiqueta
  static bool intermitencia_ant;



    if (cambio)
    {
      ReespiratorLCD.Goto(0,0);
      ReespiratorLCD.Write("                    ",20);

      ReespiratorLCD.Goto(0,2);
      ReespiratorLCD.Write("                    ",20);

      ReespiratorLCD.Goto(0,3);
      ReespiratorLCD.Write("  NO        si  <<< ",20);
    }


    if ((intermitencia != intermitencia_ant) || cambio)
    {
      ReespiratorLCD.Goto(0,1);
      if (intermitencia) ReespiratorLCD.Write("  PONER EN REPOSO?  ",20);
      else               ReespiratorLCD.Write("                    ",20);
    }



    intermitencia_ant = intermitencia;
};
