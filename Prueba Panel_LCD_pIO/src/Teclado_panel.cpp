#include "Teclado_panel.h"
#include <arduino.h>




Teclado_panel::Teclado_panel (int no_usado)
{

  pinMode (TECLA_1_pin, INPUT);
  pinMode (TECLA_2_pin, INPUT);
  pinMode (TECLA_3_pin, INPUT);
  pinMode (TECLA_4_pin, INPUT);

  pinMode (ENCODER_CLK_pin, INPUT);
  pinMode (ENCODER_DIR_pin, INPUT);
  pinMode (ENCODER_BTN_pin, INPUT);


}



int Teclado_panel::update_teclas()
{
  static unsigned long milis_filtra;
  static unsigned long milis_ant;
  static int tecla_pulsada_ant = 0;
  static int tecla_filtrada_ant = 0;

  unsigned long milisegundos;
  int tecla_pulsada  = 0;               // El número de la tecla que se está pulsando
  int tecla_filtrada = 0;               // El número de la tecla filtrada por tiempo
  int tecla_enviada  = 0;               // El numero de la tecla que se envía una vez


  milisegundos = millis();
  if (milis_ant > milisegundos) milis_filtra = 0;   // Evita errores por desbordamiento de millis()
  milis_ant = milisegundos;


  tecla_pulsada = 0;                                      // Muestrea las teclas
  if (digitalRead (TECLA_1_pin))      tecla_pulsada = 1;
  else if (digitalRead (TECLA_2_pin)) tecla_pulsada = 2;
  else if (digitalRead (TECLA_3_pin)) tecla_pulsada = 3;
  else if (digitalRead (TECLA_4_pin)) tecla_pulsada = 4;


  if (tecla_pulsada != 0)
  {
    if (tecla_pulsada == tecla_pulsada_ant)
    {
      if (milisegundos > milis_filtra)                     // valida el botón
            tecla_filtrada = tecla_pulsada;
    }
    else                                                  // Si cambio la tecla reinicia el tiempo de filtrado
            milis_filtra = milisegundos + tiempo_filtra_boton;
  }
  else tecla_filtrada = 0;

  // El numero de la tecla filtrada solo se envia una vez
  if (tecla_filtrada != tecla_filtrada_ant) tecla_enviada = tecla_filtrada;
  else tecla_enviada = 0;

  tecla_pulsada_ant  = tecla_pulsada;
  tecla_filtrada_ant = tecla_filtrada;

  return tecla_enviada;
}



bool Teclado_panel::update_tecla_encoder()
{
  static unsigned long milis_ant;
  static unsigned long milis_filtra;
  static unsigned long milis_no_lectura;
  static bool pulsado_ant = false;
  static bool filtrado_ant = false;

  unsigned long milisegundos;
  bool pulsado = false;
  bool filtrado = false;
  bool enviado;

  milisegundos = millis();
  if (milis_ant > milisegundos)              // Evita errores por desbordamiento de millis()
  {
    milis_filtra = 0;
    milis_no_lectura = 0;
  }
  milis_ant = milisegundos;


 if (milisegundos < milis_no_lectura)       // Evita dobles pulsaciones
              return false;


  pulsado = digitalRead (ENCODER_BTN_pin);
  if (pulsado)
  {
    if (pulsado_ant)
    {
      if (milisegundos > milis_filtra)   //  valida el botón si pasa el tiempo de espera corta con el mismo botón pulsado
        filtrado = true;
    }
    else
      milis_filtra = milisegundos + tiempo_filtra_boton;
  }
  else filtrado = false;


  if (filtrado && !filtrado_ant)
  {
    enviado = true;
    milis_no_lectura = milisegundos + tiempo_filtra_boton;
  }
  else enviado = false;

  pulsado_ant = pulsado;
  filtrado_ant = filtrado;

  return enviado;
}



int Teclado_panel::update_encoder()
{
  static unsigned long milis_filtra;
  static unsigned long milis_ant;
  static bool primera = true;
  static bool fl_baja = false;
  static bool fl_sube = false;
  static bool encoderCLK_ant;
  static bool encoderDIR_ant;

  unsigned long milisegundos;
  bool encoderCLK;
  bool encoderDIR;

  milisegundos = millis();
  if (milis_ant > milisegundos)              // Evita errores por desbordamiento de millis()
    milis_filtra = 0;

  milis_ant = milisegundos;


  encoderCLK = digitalRead (ENCODER_CLK_pin);
  encoderDIR = digitalRead (ENCODER_DIR_pin);
  if (primera)
  {
    encoderCLK_ant = encoderCLK;
    encoderDIR_ant = encoderDIR;
    valor_encoder = 0;
    primera = false;
  }


    if (encoderCLK_ant && !encoderCLK)          // detecta el flanco descendente
    {
      milis_filtra = milisegundos + tiempo_filta_encoder;
      fl_baja = true;
    }
    else if (fl_baja)
    {
      if ((encoderCLK) || (encoderDIR != encoderDIR_ant))   // Si cambian las señales ignora el pulso
          fl_baja = false;
      else if (milisegundos > milis_filtra)
      {
        if (encoderDIR) valor_encoder ++;
        else valor_encoder --;
        fl_baja = false;
      }
    }

  // detecta el flanco ascendente
    else if (!encoderCLK_ant && encoderCLK)
    {
      milis_filtra = milisegundos + tiempo_filta_encoder;
      fl_sube = true;
    }
    else if (fl_sube)
    {
      if ((!encoderCLK) or (encoderDIR != encoderDIR_ant))
          fl_sube = false;
      else if (milisegundos > milis_filtra)
      {
        if (encoderDIR) valor_encoder --;
        else valor_encoder ++;
        fl_sube = false;
      }
    }

      encoderCLK_ant = encoderCLK;
      encoderDIR_ant = encoderDIR;

      return valor_encoder;
}



int Teclado_panel::get_encoder()
{
  return valor_encoder;
}


void Teclado_panel::set_encoder(int nuevo_Valor_encoder)
{
  valor_encoder = nuevo_Valor_encoder;
}
