/*
  2020-04-11


  Simula el funcionamiento del REESPIRADOR a efectos de probar el PANEL_LCD

  Utiliza el nuevo driver de proceso por lotes para el LCD

     --------------------------------------------------------
     CONFIGURACION DE HARDWARE:
     LCD 20x4 líneas paralelo (descrito en PINOUT.h)
        RS       A0
        RW       A1
        E        A2
        D0       37
        ...      ...
        D7       30

      Encoder
        A       A14
        B       A15
        Boton    43

    Teclas
        1        33
        2        32
        3        31
        4        30
     --------------------------------------------------------




*/


#include "Panel_LCD.h"
#include <arduino.h>



// #include "Boton_encoder.h"


// Variables asociadas a los parámetros medidos
  int peak = 28;
    int peak_minim =  9;
    int peak_minim_alrm = 9;
    int peak_maxim =  35;
    int peak_maxim_alrm = 35;

  int peep =  6;
    int peep_minim = 4;
    int peep_minim_alrm = 4;
    int peep_maxim = 25;
    int peep_maxim_alrm = 25;

  int rpm = 15;
    int rpm_minim =  5;
    int rpm_minim_alrm = 5;
    int rpm_maxim =  30;
    int rpm_maxim_alrm = 30;

  int volumen =400;
    int vol_minim = 100;
    int vol_minim_alrm = 100;
    int vol_maxim = 900;
    int vol_maxim_alrm = 900;

  int relacion_ie = 2;       // significa una relación 1:2
    int relacion_ie_min = 4;
    int relacion_ie_max = 1;

  int alarmas_activas;

  int bits_alarmas;
         /* bit 0: VolumeLow
            bit 1: VolumeHigh
            bit 2: RPMLow
            bit 3: RPMHigh
            bit 4: PeakPressureLow
            bit 5: PeakPressureHigh
            bit 6: PeepPressureLow
            bit 7: PeepPressureHigh
            bit 8: BatteryLow
            bits 9-15: reserved   */

  int bits_estado;
         /* b0: Ventilation running
            b1: Ventilation paused
            b2: Ventilation stopped
            b3: Ventilation emergency stopped
            b4: Reserved for future implementations
            b5: Reserved for future implementations
            b6: Reserved for future implementations
            b7: Reserved for future implementations */



//            Panel_LCD monitor_LCD (A10, A11, A12, A13, A15, A14, 43);
            Panel_LCD monitor_LCD ;




void setup() {

  bits_estado = 4;        // Máquina parada
  bits_alarmas = 0;       // Sin alarmas

  Serial.begin (115200);

// Inicializa el panel_LCD. Para la versión definitiva hay que definir el
        // mecanismo por el que el panel consigue los parámetros

  monitor_LCD.init();


  Serial. println (monitor_LCD.medidas [0].etiqueta);
  Serial. println (monitor_LCD.medidas [1].etiqueta);
  Serial. println (monitor_LCD.medidas [2].etiqueta);
  Serial. println (monitor_LCD.medidas [3].etiqueta);
  Serial. println (monitor_LCD.linea_etiquetas);


  Serial. println (monitor_LCD.linea_valores);
  Serial. println ();

  for(int i=0; i<16; i++)
  {
    Serial.print (i);
    Serial.print("\t");
    Serial.println (monitor_LCD.alarmas[i]);
  }
  Serial. println ();
  Serial. println ();


}

void loop() {

  static unsigned long micros_antes, micros_update;
  static unsigned long milisegundos_fin_insp, milisegundos_fin_exp;
  static unsigned long micros_minimo, micros_maximo, media, contador = 0;

  bool fin_insp = false, fin_exp = false;


  static int tiempo_ciclo_resp;
    tiempo_ciclo_resp = 60000 / rpm;

    if (millis() > 5000)  monitor_LCD.fin_de_inicio ();



// Simula un ciclo respiratorio
    if (millis () > milisegundos_fin_insp)
    {
      peak += random (-4,5);
      peak = constrain (peak, peak_minim, peak_maxim);

      milisegundos_fin_insp = millis() + tiempo_ciclo_resp;
      milisegundos_fin_exp  = millis() + tiempo_ciclo_resp / 2;
      fin_insp = true;
    }

    if (millis () > milisegundos_fin_exp)
    {
      peep += random (-1, 2);
      peep= constrain (peep, peep_minim, peep_maxim);

      volumen += random (-25, 26);
      volumen = constrain (volumen, vol_minim, vol_maxim);

      rpm += random (-1,2);
      rpm = constrain (rpm, rpm_minim, rpm_maxim);

      tiempo_ciclo_resp = 60000 / rpm;
      milisegundos_fin_insp = millis() + tiempo_ciclo_resp / 2;
      milisegundos_fin_exp  = millis() + tiempo_ciclo_resp;
      fin_exp = true;
    }



    peak_minim_alrm = monitor_LCD.medidas [0].alarma_minimo;
    peak_maxim_alrm = monitor_LCD.medidas [0].alarma_maximo;

    peep_minim_alrm = monitor_LCD.medidas [1].alarma_minimo;
    peep_maxim_alrm = monitor_LCD.medidas [1].alarma_maximo;

    rpm_minim_alrm  = monitor_LCD.medidas [2].alarma_minimo;
    rpm_maxim_alrm  = monitor_LCD.medidas [2].alarma_maximo;

    vol_minim_alrm  = monitor_LCD.medidas [3].alarma_minimo;
    vol_maxim_alrm  = monitor_LCD.medidas [3].alarma_maximo;

// Calcula la palabra de alarmas;
  alarmas_activas = 0;

  if (volumen < monitor_LCD.medidas [3].alarma_minimo) alarmas_activas += 1;
  if (volumen > monitor_LCD.medidas [3].alarma_maximo) alarmas_activas += 2;

  if (rpm < monitor_LCD.medidas [2].alarma_minimo) alarmas_activas += 4;
  if (rpm > monitor_LCD.medidas [2].alarma_maximo) alarmas_activas += 8;

  if (peak < monitor_LCD.medidas [0].alarma_minimo) alarmas_activas += 16;
  if (peak > monitor_LCD.medidas [0].alarma_maximo) alarmas_activas += 32;

  if (peep < monitor_LCD.medidas [1].alarma_minimo) alarmas_activas += 64;
  if (peep > monitor_LCD.medidas [1].alarma_maximo) alarmas_activas += 128;



  micros_antes= micros();
    monitor_LCD.update(peak, peep, rpm, volumen, alarmas_activas);
  micros_update = micros() - micros_antes;

  contador ++;
  micros_minimo = min (micros_minimo, micros_update);
  micros_maximo = max (micros_maximo, micros_update);
  media += micros_update;


  if (fin_insp || fin_exp)
  {
    if (fin_insp) Serial.print ("fin insp  ");
    else Serial.print ("fin exp*  ");

    Serial.print ("\t");
    Serial.print (millis());
    Serial.print ("\t");
    Serial.print (tiempo_ciclo_resp);
    Serial.print ("\t");
    Serial.print (monitor_LCD.estado_display );
    Serial.print ("\t");
    Serial.print (micros_update);
    Serial.print ("\t");
    Serial.print (alarmas_activas,BIN);

    if (fin_exp)
    {
        Serial.print ("\tmin = ");
        Serial.print (micros_minimo);
        Serial.print ("\tmax = ");
        Serial.print (micros_maximo);
        Serial.print ("\tmedia = ");
        Serial.print (media/contador);
        Serial.println ("\t microseg");
            micros_minimo = 99999999;
            micros_maximo = 0;
            media = 0;
            contador = 0;
    }
    else Serial.println ();

  }

delay (1);
}
