#ifndef PINOUT_TECLADO_H
#define PINOUT_TECLADO_H

#define TECLA_1_pin  A10
#define TECLA_2_pin  A11
#define TECLA_3_pin  A12
#define TECLA_4_pin  A13

#define ENCODER_CLK_pin  A14       // terminales del encoder
#define ENCODER_DIR_pin  A15       // terminales del encoder
#define ENCODER_BTN_pin  43        // terminales del encoder

#endif
