#ifndef TECLADO_PANEL_H
#define TECLADO_PANEL_H


#include "Teclado_PINOUT.h"


// Clase que maneja las teclas y el encoder del panel de mando con LCD

class Teclado_panel{

  public:

    Teclado_panel(int BOTON_pin);

    int update_teclas ();                       // Muestrea las teclas. Resultado ≠ 0 solo en un ciclo
    bool update_tecla_encoder();                // Muestrea la tecla del encoder
    int update_encoder();                       // Muestrea el encoder y lee el valor
    int get_encoder();                          // Obtiene el valor actual del encoder
    void set_encoder (int);                     // Fijka un valor para el encoder









//  private:

    int valor_encoder;

    int tiempo_filtra_boton = 30;               // Tiempo de filtrado milis
    int tiempo_filta_encoder = 2;               // Tiempo de filtrado del encoder
};


#endif
