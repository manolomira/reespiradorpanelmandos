#include <Arduino.h>
#include "Teclado_panel.h"




/*
    int update_teclas ();                       // Muestrea las teclas. Resultado ≠ 0 solo en un ciclo
    bool update_tecla_encoder();                // Muestrea la tecla del encoder
    int update_encoder();                       // Muestrea el encoder y lee el valor
    int get_encoder();                          // Obtiene el valor actual del encoder
    void set_encoder (int);
*/

int tecla_pulsada;
Teclado_panel teclado_panel(10);


void setup() {
  // put your setup code here, to run once:

  Serial.begin (115200);

  teclado_panel.set_encoder(0);
}

void loop() {

  int  tecla_pulsada = teclado_panel.update_teclas();
  bool encoder_pulsado = teclado_panel.update_tecla_encoder();
  int  posicion = teclado_panel.update_encoder();
  static int posicion_ant;

if (tecla_pulsada != 0)
{
  Serial.print (millis()); Serial.print ("  --  "); Serial.println (tecla_pulsada);
}

if (encoder_pulsado)
{
  Serial.print (millis()); Serial.print ("  encoder --  "); Serial.println (encoder_pulsado);
}

if (posicion != posicion_ant)
{
  Serial.print (millis()); Serial.print ("  posicion encoder --  "); Serial.println (posicion);
}

posicion_ant = posicion;

delay (1);
  // put your main code here, to run repeatedly:
}
