#include "Boton_encoder.h"




Boton_encoder::Boton_encoder(int BOTON_pin)
{
  _BOTONpin = BOTON_pin;
  pinMode (_BOTONpin, INPUT);
}

bool Boton_encoder::muestreo()
{
  static unsigned long milisegundos_confirma_pulsacion;
  static bool boton_pulsado_ant;

  bool boton_pulsado = false;
  bool boton_pulsado_aceptado = false;
  unsigned long milisegundos;
  milisegundos = millis();
  
  boton_pulsado = digitalRead (_BOTONpin);

  if (boton_pulsado)
  {
    if (boton_pulsado_ant)
    {
      if (milisegundos > milisegundos_confirma_pulsacion)   //  valida el botón si pasa el tiempo de espera corta con el mismo botón pulsado
      {
        boton_pulsado_aceptado = boton_pulsado;
      }
    }
    else
    {
      milisegundos_confirma_pulsacion = milisegundos + tiempo_confirma_pulsacion;
    }    
  }


  boton_pulsado_ant = boton_pulsado;
  return boton_pulsado_aceptado;
//  return boton_pulsado;
}
