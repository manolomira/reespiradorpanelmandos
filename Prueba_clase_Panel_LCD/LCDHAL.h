#ifndef LCDHAL_H
#define LCDHAL_H

#include <stdio.h>


#define LCDBufferSize 128

typedef struct
{
  unsigned char Data;
  unsigned Command:1;
  unsigned :7;
}sLCDBuffer;

typedef struct
{
  unsigned BufferFull:1;
  unsigned BufferEmpty:1;
  unsigned :6;
}sLCDFlags;

class LCDHAL
{
  public:
  LCDHAL(void);
  ~LCDHAL(void);
  bool LCDBufferFull(void);
  bool LCDBufferEmpty(void);
  bool LCDGoto(unsigned char Column, unsigned char Row);
  bool LCDInit(void);
  bool LCDClear();
  bool LCDWrite(const char *OutputString,unsigned char Size);
  bool LCDPutchar(char LCDChar);
  void Update(void);
  private:
  FILE _LCDOut={0};
  FILE _Oristdout;
  const /*PROGMEM*/ unsigned char _RowOffset[4]={0x80,0xC0,0x94,0xD4};
  sLCDFlags _LCDFlags={false,true};
  sLCDBuffer _LCDBuffer[LCDBufferSize];
  bool _LCDBufferPush(sLCDBuffer *LCDCommand);
  sLCDBuffer* _LCDBufferPop(void);
  void _LCDPrint(sLCDBuffer *LCDData);
  unsigned char _LCDBufferInputIndex=0,_LCDBufferOutputIndex=0;
  static LCDHAL* _LCDInstance;
  static int _LCDPutchar(char c, FILE *stream);
  static int (*_oriPutchar)(char, FILE *);
};

#endif
