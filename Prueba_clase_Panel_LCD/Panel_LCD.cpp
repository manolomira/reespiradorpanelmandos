#include "Panel_LCD.h"
#include <Arduino.h>


  LCDHAL ReespiratorLCD;                                  // Crea el LCD con un objeto LCDHAL
  Boton_encoder boton_encoder(43);                        // Crea una instancia para leer el botón del encoder



Panel_LCD::Panel_LCD (int BUTTON1pin, int BUTTON2pin, int BUTTON3pin, int BUTTON4pin, int ecoderCLKPin, int ecoderDIRPin, int ecoderBOTPin)
{
   _BUTTONpin [1] = BUTTON1pin;
   _BUTTONpin [2] = BUTTON2pin;
   _BUTTONpin [3] = BUTTON3pin;
   _BUTTONpin [4] = BUTTON4pin;

   _encoderCLKpin = ecoderCLKPin;
   _encoderDIRpin = ecoderDIRPin;
   _encoderBOTpin = ecoderBOTPin; 
};



void Panel_LCD::init()                        // Inicializa el display. Carga los menús de variables para editar

{
    milisegundos_ant = millis();  
    estado_display = State_Init;  

    ReespiratorLCD.LCDInit();                 // Inicializa el display
    ReespiratorLCD.LCDClear();

    
    for (int i=1; i<5; i++)                   // Establece el modo de los pines asociados a los botones
    {
      pinMode (_BUTTONpin [1], INPUT);
    }
              
  // Variables principales del sistema
      numParametros = 6;
                      
            sprintf (medidas [0].etiqueta, " PEAK");
            medidas [0].valor_consigna = 28;
            medidas [0].valor_minimo =   9;
            medidas [0].valor_maximo =   35;
            medidas [0].valor_instantaneo = medidas [0].valor_consigna;
            medidas [0].alarma_minimo = medidas [0].valor_minimo;
            medidas [0].alarma_maximo = medidas [0].valor_maximo;
            
            sprintf (medidas [1].etiqueta, " PEEP");
            medidas [1].valor_consigna = 6;       // tiempo recruit
            medidas [1].valor_minimo =   4;
            medidas [1].valor_maximo =   25;
            medidas [1].valor_instantaneo = medidas [1].valor_consigna;
            medidas [1].alarma_minimo = medidas [1].valor_minimo;
            medidas [1].alarma_maximo = medidas [1].valor_maximo;
            
            sprintf (medidas [2].etiqueta, "  RPM");
            medidas [2].valor_consigna = 15;
            medidas [2].valor_minimo =   5;
            medidas [2].valor_maximo =   30;
            medidas [2].valor_instantaneo = medidas [2].valor_consigna;
            medidas [2].alarma_minimo = medidas [2].valor_minimo;
            medidas [2].alarma_maximo = medidas [2].valor_maximo;
            
            sprintf (medidas [3].etiqueta, "  VOL");
            medidas [3].valor_consigna = 400;
            medidas [3].valor_minimo =   100;
            medidas [3].valor_maximo =   900;
            medidas [3].valor_instantaneo = medidas [3].valor_consigna;
            medidas [3].alarma_minimo = medidas [3].valor_minimo;
            medidas [3].alarma_maximo = medidas [3].valor_maximo;
            
            sprintf (medidas [4].etiqueta, " trig");
            medidas [4].valor_consigna = 10;
            medidas [4].valor_minimo =   1;
            medidas [4].valor_maximo =   20;
            medidas [4].valor_instantaneo = 0;     // 0 => off; > 0 => on
            
            sprintf (medidas [5].etiqueta, "RECRU");
            medidas [5].valor_consigna = 10;       // tiempo recruit
            medidas [5].valor_minimo =   1;
            medidas [5].valor_maximo =   20;
            medidas [5].valor_instantaneo = 0;     // 0 => off; > 0 => on
    
    
    
  // Alarmas y otras variables del sistema
      numParametrosSec = 9;
    
            sprintf (parametro_sec[0].etiqueta, "PEAK_consigna");
            parametro_sec[0].valor_consigna = medidas [0].valor_consigna;
            parametro_sec[0].valor_minimo   = medidas [0].valor_minimo;
            parametro_sec[0].valor_maximo   = medidas [0].valor_maximo;
        
            sprintf (parametro_sec[1].etiqueta, "ALRM_PEAK_bajo");
            parametro_sec[1].valor_consigna = medidas [0].alarma_minimo;
            parametro_sec[1].valor_minimo   = medidas [0].valor_minimo;
            parametro_sec[1].valor_maximo   = medidas [0].alarma_maximo;
        
            sprintf (parametro_sec[2].etiqueta, "ALRM_PEAK_alto");
            parametro_sec[2].valor_consigna = medidas [0].alarma_maximo;
            parametro_sec[2].valor_minimo   = medidas [0].alarma_minimo;
            parametro_sec[2].valor_maximo   = medidas [0].valor_maximo;
        
        
            sprintf (parametro_sec[3].etiqueta, "PEEP_consigna");
            parametro_sec[3].valor_consigna = medidas [1].valor_consigna;
            parametro_sec[3].valor_minimo   = medidas [1].valor_minimo;
            parametro_sec[3].valor_maximo   = medidas [1].valor_maximo;
        
            sprintf (parametro_sec[4].etiqueta, "ALRM_PEEP_bajo");
            parametro_sec[4].valor_consigna = medidas [1].alarma_minimo;
            parametro_sec[4].valor_minimo   = medidas [1].valor_minimo;
            parametro_sec[4].valor_maximo   = medidas [1].alarma_maximo;
        
            sprintf (parametro_sec[5].etiqueta, "ALRM_PEEP_alto");
            parametro_sec[5].valor_consigna = medidas [1].alarma_maximo;
            parametro_sec[5].valor_minimo   = medidas [1].alarma_minimo;
            parametro_sec[5].valor_maximo   = medidas [1].valor_maximo;
        
        
            sprintf (parametro_sec[6].etiqueta, " RPM_consigna");
            parametro_sec[6].valor_consigna = medidas [2].valor_consigna;
            parametro_sec[6].valor_minimo   = medidas [2].valor_minimo;
            parametro_sec[6].valor_maximo   = medidas [2].valor_maximo;
        
            sprintf (parametro_sec[7].etiqueta, "ALRM_RPM_bajo");
            parametro_sec[7].valor_consigna = medidas [2].alarma_minimo;
            parametro_sec[7].valor_minimo   = medidas [2].valor_minimo;
            parametro_sec[7].valor_maximo   = medidas [2].alarma_maximo;
        
            sprintf (parametro_sec[8].etiqueta, "ALRM_RPM_alto");
            parametro_sec[8].valor_consigna = medidas [2].alarma_maximo;
            parametro_sec[8].valor_minimo   = medidas [2].alarma_minimo;
            parametro_sec[8].valor_maximo   = medidas [2].valor_maximo;
        
            sprintf (parametro_sec[9].etiqueta, "REPOSO RESPIR");
            parametro_sec[9].valor_consigna = 0;
            parametro_sec[9].valor_minimo   = 0;
            parametro_sec[9].valor_maximo   = 1;








// Rellena la linea de etiquetas
        for (int i = 0; i<5; i++)
        {
          linea_etiquetas[i]    = medidas [0].etiqueta[i];
          linea_etiquetas[i+ 5] = medidas [1].etiqueta[i];
          linea_etiquetas[i+10] = medidas [2].etiqueta[i];
          linea_etiquetas[i+15] = medidas [3].etiqueta[i];
        }




  sprintf (linea_alarmas, "(3) ALRM CIRCULANTE ");

                  
}






void Panel_LCD::fin_de_inicio (void)                // Fin de inicio. Sirve para que el sistema avise del final del inicio
{
  if (estado_display == State_Init) estado_display = State_Idle;
}



void Panel_LCD::actualiza_linea_alarmas()
{
  // Establece el valor de la variable char[] que se mostrara en la línea de alarmas

  sprintf (linea_alarmas, "(3) ALRM CIRCULANTE ");

  linea_alarmas_actualizada = true;
}








//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
int Panel_LCD::muestreo_boton_pulsado()
{
  int boton_pulsado = 0;
  static int boton_pulsado_ant = 0;
  
  int boton_aceptado = 0;
  static int boton_aceptado_ant = 0;
  
  int boton_enviado = 0;                                // Es el numero de boton pulsado y aceptado. se emite solo en un ciclo
  unsigned long milisegundos_confirma_pulsacion_corta;

  
  boton_pulsado = 0;
  if (digitalRead (_BUTTONpin [1])) boton_pulsado = 1;
  else if (digitalRead (_BUTTONpin [2])) boton_pulsado = 2;
  else if (digitalRead (_BUTTONpin [3])) boton_pulsado = 3;
  else if (digitalRead (_BUTTONpin [4])) boton_pulsado = 4;

  if (boton_pulsado != 0)
  {
    if ((boton_pulsado == boton_pulsado_ant) &&
        (milisegundos > milisegundos_confirma_pulsacion_corta))   //  valida el botón si pasa el tiempo de espera corta con el mismo botón pulsado
    {
      boton_aceptado = boton_pulsado;
    }
    else
    {
      milisegundos_confirma_pulsacion_corta = milisegundos + boton_espera_corta;
    }    
  }
  else boton_aceptado = 0;

  if (boton_aceptado != boton_aceptado_ant) boton_enviado = boton_aceptado;
  else boton_enviado = 0;
  
  boton_pulsado_ant = boton_pulsado;
  boton_aceptado_ant = boton_aceptado;

  return boton_enviado;
}





//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
int Panel_LCD::muestreo_encoder()
{
  static bool _encoderBUT, _encoderBUT_ant;
  static bool movimientoEncoder_baja = false, movimientoEncoder_sube = false;
  static unsigned long milisConfirEncoder;

  _encoderCLK = digitalRead (_encoderCLKpin);
  _encoderDIR = digitalRead (_encoderDIRpin);
  _encoderBUT = digitalRead (_encoderBOTpin);

  // detecta el flanco descendente
    if (_encoderCLK_ant and !_encoderCLK)       
    {
      milisConfirEncoder = milisegundos + TiempoFiltroEncoder;
      movimientoEncoder_baja = true;
    }
  
    else if (movimientoEncoder_baja)
    {
      if ((_encoderCLK) or (_encoderDIR != _encoderDIR_ant)) movimientoEncoder_baja = false;
      else if (milisegundos > milisConfirEncoder)
      {
        if (_encoderDIR) _encoderValor --;
        else _encoderValor ++;
        movimientoEncoder_baja = false;
      }
    }

  // detecta el flanco ascendente
    else if (!_encoderCLK_ant and _encoderCLK)       
    {
      milisConfirEncoder = milisegundos + TiempoFiltroEncoder;
      movimientoEncoder_sube = true;
    }
  
    else if (movimientoEncoder_sube)
    {
      if ((!_encoderCLK) or (_encoderDIR != _encoderDIR_ant)) movimientoEncoder_sube = false;
      else if (milisegundos > milisConfirEncoder)
      {
        if (_encoderDIR) _encoderValor ++;
        else _encoderValor --;
        movimientoEncoder_sube = false;
      }
    }

      _encoderCLK_ant = _encoderCLK;
      _encoderDIR_ant = _encoderDIR;
      return _encoderValor;
}



//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
int Panel_LCD::get_lectura_encoder()
{
  return _encoderValor;
}



//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
//                -----------  TRASLADAR A UNA CLASE INDEPENDIENTE  -----------
void Panel_LCD::set_lectura_encoder(int nuevo_Valor_encoder)
{
  _encoderValor = nuevo_Valor_encoder;
}







void Panel_LCD::update (int valor0, int valor1, int valor2, int valor3)
{
  static int estado_display_ant;
  static unsigned long millis_refresco_pantalla, milisegundos_fin_edicion;
  static bool cambio_estado = true;

  int boton_seleccionado;
  bool refresca_pantalla = false;
  static int consigna_nueva_ant;
  static int consigna_ant, consigna_nueva;
  static int edicion_linea = 0;
  static bool edicion_parametro = false;
  bool pulsa_boton;


  milisegundos = millis();




  cambio_estado = estado_display != estado_display_ant;    // Determina su en la última iteración cambió el estado del display
  estado_display_ant = estado_display;
  
  if (cambio_estado)                                       // Fuerza la actualización de la pantalla cuando hay cambio de estado
  {
    linea_consignas_actualizada = true;
    linea_valores_actualizada = true;
    linea_alarmas_actualizada = true;

    refresca_pantalla = true;
  }

  if (milisegundos > millis_refresco_pantalla)             // Fuerza la actualización de la pantalla cada cierto tiempo
  {
    millis_refresco_pantalla = milisegundos + intervaloRefresco;
    refresca_pantalla = true;
  }

  if (refresca_pantalla)                                  // Si refresca pantalla toma los últimos valores
  {                                                       // Solo detecta los cambios de valor cuando va a renovar la pantalla.
    medidas [0].valor_instantaneo = valor0;
    medidas [1].valor_instantaneo = valor1;
    medidas [2].valor_instantaneo = valor2;
    medidas [3].valor_instantaneo = valor3;
    

    for (int i=0; i<5; i++)
    {
      if (medidas [i].valor_instantaneo_anterior != medidas [i].valor_instantaneo) linea_valores_actualizada = true;

      medidas [i].valor_instantaneo_anterior = medidas [i].valor_instantaneo;
    }
            if (linea_valores_actualizada)
            {
              sprintf (linea_valores, " %4i %4i %4i %4i\0", 
                      medidas [0].valor_instantaneo,
                      medidas [1].valor_instantaneo,
                      medidas [2].valor_instantaneo,
                      medidas [3].valor_instantaneo);
            }
  }


  // Actualiza los perifericos
  ReespiratorLCD.Update();                                  // actualiza el buffer del display
  boton_seleccionado = muestreo_boton_pulsado();            // muestrea el teclado
  muestreo_encoder();                                       // muestrea el encoder
  pulsa_boton = boton_encoder.muestreo();

   




    


   switch (estado_display) {                                                // Solo se consideran estados 10:normal y 11: edición
        case State_Init:                // equipo iniciando
        {
          
          // Actualiza la pantalla
                if (refresca_pantalla) Pantalla_State_Init (true);                              // Dibuja la pantalla "normal"                    
        }
        break;


        case  State_Idle:                // configuración parado
        {
             parametro_conf = 0;
             consigna_nueva = 0;
             consigna_nueva_ant = 0;
             edicion_linea = 0;

            // Transiciones asociadas con los botones
                if ((boton_seleccionado == 1)|| (boton_seleccionado == 2))                   // Cambia al estado de edicion si se pulsa un botón
                {
                  estado_display = State_Conf_Idle;
                }

                else if ((boton_seleccionado == 3)|| (boton_seleccionado == 4))              // RSe pone en marcha si se pulsa un botón
                {
                  estado_display = State_Working_PC;
                }                
  
            // Actualiza la pantalla
                if (refresca_pantalla) Pantalla_State_Idle (cambio_estado);                  // Dibuja la pantalla configuración parado                    
        }
        break;


        case  State_Conf_Idle:                // configuración parametro parado
        {

            // Cálculos asociados con los botones
                if (boton_seleccionado == 1)                                                 // configura el valor de consigna
                {
                  set_lectura_encoder (medidas [parametro_conf].valor_consigna);
                  edicion_linea = 1;
                }

               else if (boton_seleccionado == 2)                                                  // configura el valor de alarma por mínimo
                {
                  set_lectura_encoder (medidas [parametro_conf].alarma_minimo);
                  edicion_linea = 2;
                }

                else if (boton_seleccionado == 3)                                                 //configura el valor de alarma por maximo
                {
                  set_lectura_encoder (medidas [parametro_conf].alarma_maximo);
                  edicion_linea = 3;
                }

                else if (boton_seleccionado == 4)              // Cambia al estado de edicion si se pulsa un botón
                {
                  set_lectura_encoder (0);
                  if (edicion_linea != 0) edicion_linea = 0;
                }

            // Cálculos asociados con el encoder                  
                if (refresca_pantalla)
                {
                  if (edicion_linea == 0)
                  {
                    if (muestreo_encoder() > 0) parametro_conf ++;
                    if (muestreo_encoder() < 0) parametro_conf --;
                    
                    if (parametro_conf <  0) parametro_conf = numParametros -1;
                    if (parametro_conf >= numParametros) parametro_conf =  0;
                    
                    set_lectura_encoder (0);
                  }
                
                  else if (edicion_linea == 1)
                  {
                    consigna_nueva = muestreo_encoder();                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (consigna_nueva != consigna_nueva_ant)
                    {
                      consigna_nueva = constrain (consigna_nueva, 
                                                    medidas [parametro_conf].alarma_minimo, 
                                                    medidas [parametro_conf].alarma_maximo);
                      set_lectura_encoder (consigna_nueva);
                      medidas [parametro_conf].valor_consigna = consigna_nueva;
                      consigna_nueva_ant = consigna_nueva;
                    }
                  }
  
                  else if (edicion_linea == 2)
                  {
                    consigna_nueva = muestreo_encoder();                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (consigna_nueva != consigna_nueva_ant)
                    {
                      consigna_nueva = constrain (consigna_nueva, 
                                                    medidas [parametro_conf].valor_minimo, 
                                                    medidas [parametro_conf].alarma_maximo-5);
                      set_lectura_encoder (consigna_nueva);
                      medidas [parametro_conf].alarma_minimo = consigna_nueva;
                      consigna_nueva_ant = consigna_nueva;
                    }
                  }
  
                  else if (edicion_linea == 3)
                  {
                    consigna_nueva = muestreo_encoder();                           // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                    if (consigna_nueva != consigna_nueva_ant)
                    {
                      consigna_nueva = constrain (consigna_nueva, 
                                                    medidas [parametro_conf].alarma_minimo+5, 
                                                    medidas [parametro_conf].valor_maximo);
                      set_lectura_encoder (consigna_nueva);
                      medidas [parametro_conf].alarma_maximo = consigna_nueva;
                      consigna_nueva_ant = consigna_nueva;
                    }                      
                  }
                }

            // Transiciones asociadas con los botones
                if ((boton_seleccionado == 4) && (edicion_linea == 0)) estado_display = State_Idle;        // Va a estado State_Idle

            // Actualiza la pantalla
                if (refresca_pantalla) 
                 {
                   Pantalla_State_Conf_Idle (cambio_estado, edicion_linea);                              // Dibuja la pantalla configuración parado                    
                 }
        }
        break;

        
        /*************************************************************
         *          STATE_WORKING_PC                                 *
         *************************************************************/
        case  State_Working_PC:                // estado normal
        {
//          if (cambio_estado) {}; 

            // Cálculos asociados con los botones
                  ultimo_boton_seleccionado = 0;
                  if (boton_seleccionado != 0)                                    // Cambia al estado de edicion si se pulsa un botón
                  {
                    if (boton_seleccionado <= 3)
                    {
                      
                      boton_pulsado_seleccionado = boton_seleccionado;
                      milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;     
                      ultimo_boton_seleccionado = boton_seleccionado;
                      consigna_ant = medidas[ultimo_boton_seleccionado-1].valor_consigna;
                      set_lectura_encoder(consigna_ant);
  
                      estado_display = State_Working_PC_edit_1;
                    }
                    else if (boton_seleccionado == 4)
                    {
                      estado_display = State_Working_PC_edit_2;
                    }
                  }
                
            // Actualiza la pantalla
                 if (refresca_pantalla) Pantalla_State_Working_PC (cambio_estado);                // Dibuja la pantalla "normal"
        }
        break;


        /*************************************************************
         *          STATE_WORKING_PC_EDIT_BASICO                     *
         *************************************************************/
        case  State_Working_PC_edit_1:                // edicion parámetro ajuste
        {          
//          if (cambio_estado) milisegundos_sin_interm = 0; 

            // Cálculos asociados con los botones
                if (boton_seleccionado == boton_pulsado_seleccionado)                        // Si se pulsa el mismo botón extiende el tiempo de edición
                            milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;

            // Cálculos asociados con el encoder
                  consigna_nueva = muestreo_encoder();                                       // Si se modificó el encoder muestra el valor y extiende el tiempo de edición
                  if (consigna_nueva != consigna_nueva_ant)
                  {
                    consigna_nueva = constrain (consigna_nueva, 
                                        medidas [ultimo_boton_seleccionado-1].valor_minimo, 
                                        medidas [ultimo_boton_seleccionado-1].valor_maximo);
                    set_lectura_encoder (consigna_nueva);
                    consigna_nueva_ant = consigna_nueva;
                    
                    milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;         // extiende el tiempo de edcición
                    milisegundos_sin_interm = milisegundos + 1000;
                  }   
                  
            // Cálculos asociados al botón del encoder
                  if (pulsa_boton)
                  {
                    medidas [ultimo_boton_seleccionado-1].valor_consigna = consigna_nueva;
                    milisegundos_fin_edicion = milisegundos + 4000;
                    estado_display = State_Working_PC_edit_1_conf;
                  }
            
            // Transiciones asociadas con los botones
                  if ((boton_seleccionado != boton_pulsado_seleccionado) &&                  // Si se pulsa otro botón vuelve al estado normal
                      (boton_seleccionado != 0)) 
                              estado_display = State_Working_PC;
                
            // Transiciones asociadas al tiempo                  
                  else if (milisegundos > milisegundos_reposo_edicion)                      // Si pasa el tiempo máximo de edición vuelve al menú normal
                                estado_display = State_Working_PC;
  
            // Actualiza la pantalla
                   if (refresca_pantalla)                                                   // Dibuja la pantalla "edicion"
                        Pantalla_State_Working_PC_edit_1 (cambio_estado, consigna_ant, consigna_nueva);                             
        }
        break;



        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_EDIT_1 (BASICO)        *
         *************************************************************/
        case State_Working_PC_edit_1_conf:                // fin edicion parámetro ajuste
        {
          if (cambio_estado) {}

            // Cálculos asociados con los botones

            // Cálculos asociados con el encoder
                  
            // Cálculos asociados al botón del encoder

            // Transiciones asociadas con los botones
                  if ((boton_seleccionado != boton_pulsado_seleccionado) &&                               // Si se pulsa otro botón vuelve al estado normal
                      (boton_seleccionado != 0)) estado_display = State_Working_PC;
                
            // Transiciones asociadas al tiempo
                  if (milisegundos > milisegundos_fin_edicion)  estado_display = State_Working_PC;        // Si pasa el tiempo máximo de edición vuelve al menú normal
  
            // Actualiza la pantalla
                   if (refresca_pantalla)                                                                  // Dibuja la pantalla "edicion"
                          Pantalla_State_Working_PC_edit_1 (cambio_estado, consigna_ant, consigna_nueva);
        }
        break;
        

        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_EDIT_2 (AVANZADO)      *
         *************************************************************/
        case  State_Working_PC_edit_2:                // fin edicion parámetro ajuste
        {
          if (cambio_estado)
          {
            edicion_parametro = false;                                            // modo de selección de valor a editar
            parametro_conf = 0;                                                   // Selecciona el primer parámetro
                consigna_nueva = parametro_sec[parametro_conf].valor_consigna;
                consigna_nueva_ant = consigna_nueva;
            milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;      // Tiempo de edicion
            milisegundos_sin_interm = 0;                                          // Parpadea desde el pricipio
          }


            // Cálculos asociados con los botones
                consigna_nueva_ant = consigna_nueva;
                if (!edicion_parametro)                                            // Modo de selección de parámetro
                {
                      if (refresca_pantalla)                                       // Solo actualiza el cambio de parámetro cuando actualiza pantalla
                      {
                        int valor_encoder = muestreo_encoder();
                        int parametro_conf_ant = parametro_conf;
                        if (valor_encoder > 0) parametro_conf ++;
                        else if (valor_encoder < 0) parametro_conf --;
        
                        if (valor_encoder != 0)
                        {
                          milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;
                          milisegundos_sin_interm = milisegundos + 1000;                  
                        }
                        
                        parametro_conf = constrain (parametro_conf, 0, numParametrosSec-1);                    
                        set_lectura_encoder (0);
            
                        if (parametro_conf != parametro_conf_ant) consigna_nueva = parametro_sec[parametro_conf].valor_consigna;
                      }                  
                }
                else                                                                // Modo de edicion de parámetro
                {
                      consigna_nueva = muestreo_encoder();                           // Si se modificó el encoder muestra el valor
                      if (consigna_nueva != consigna_nueva_ant)
                      {
                        consigna_nueva = constrain (consigna_nueva, 
                                                      parametro_sec[parametro_conf].valor_minimo, 
                                                      parametro_sec[parametro_conf].valor_maximo);
                        set_lectura_encoder (consigna_nueva);
                        
                        milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;           // extiende el tiempo de edcición
                        milisegundos_sin_interm = milisegundos + 1000;
                      }
                 } 

            // Cálculos asociados con el encoder
                  
            // Cálculos asociados al botón del encoder
                if (pulsa_boton)
                {
                  if (!edicion_parametro)                                          // pasa a modo edición
                  {
                    edicion_parametro = true ;
                    set_lectura_encoder (consigna_nueva);
                  }
                  else                                                            // graba el nuevo valor y pasa a modo seleccion
                  {  
      
                    if (parametro_conf == numParametrosSec-1)
                    {
                      if (consigna_nueva == 1)  estado_display = State_Confirm_gotoIdle;
                    }
                    else
                    {
                      edicion_parametro = false ;
                      set_lectura_encoder (0);
                      parametro_sec[parametro_conf].valor_consigna = consigna_nueva;
                      
                                                                                    // graba los nuevo valores en la tabla principal
                      if (parametro_conf == 0) medidas [0].valor_consigna = consigna_nueva;         // Valores PEAK
                      else if (parametro_conf == 1) medidas [0].alarma_minimo = consigna_nueva;
                      else if (parametro_conf == 2) medidas [0].alarma_maximo = consigna_nueva;
                      
                      else if (parametro_conf == 3) medidas [1].valor_consigna = consigna_nueva;    // Valores PEEP
                      else if (parametro_conf == 4) medidas [1].alarma_minimo = consigna_nueva;
                      else if (parametro_conf == 5) medidas [1].alarma_maximo = consigna_nueva;
                      
                      else if (parametro_conf == 6) medidas [2].valor_consigna = consigna_nueva;    // Valores RPM
                      else if (parametro_conf == 7) medidas [2].alarma_minimo = consigna_nueva;
                      else if (parametro_conf == 8) medidas [2].alarma_maximo = consigna_nueva;
                    }
                  
                  }
                  milisegundos_reposo_edicion = milisegundos + tiempo_max_edicion;
                  milisegundos_sin_interm = milisegundos + 1000;
                }     


            // Transiciones asociadas con los botones
                if (boton_seleccionado != 0) estado_display = State_Working_PC;    // Si se pulsa algún botón vuelve al estado normal
                
            // Transiciones asociadas al tiempo                  
                else if (milisegundos > milisegundos_reposo_edicion)  estado_display = State_Working_PC; // Si pasa el tiempo máximo de edición vuelve al menú normal
  
            // Actualiza la pantalla
                if (refresca_pantalla) Pantalla_State_Working_PC_edit_2 (cambio_estado, edicion_parametro, parametro_conf, consigna_nueva);                             // Dibuja la pantalla "edicion"
        }
        break;
    
    
        /*************************************************************
         *          STATE_WORKING_PC_CONFIRMA_EDIT_2 (AVANZADO)      *
         *************************************************************/
        case State_Confirm_gotoIdle:                // fin edicion parámetro ajuste
        {          
          if (cambio_estado) milisegundos_fin_edicion = milisegundos + tiempo_max_edicion;     // Tiempo de edicion

            // Transiciones asociadas con los botones
                if ((boton_seleccionado > 0) & (boton_seleccionado != 3)) estado_display = State_Working_PC;          // Si se pulsa algún botón vuelve al estado normal
                else if (boton_seleccionado == 3) estado_display = State_Idle;
                
            // Transiciones asociadas al tiempo                  
                else if (milisegundos > milisegundos_fin_edicion)  estado_display = State_Working_PC;                // Si pasa el tiempo máximo de edición vuelve al menú normal
  
            // Actualiza la pantalla
                  Pantalla_State_Confirm_gotoIdle (cambio_estado);
        }
        break;
            
            
        default:
        {
            estado_display = State_Idle;
        }
        break;
    };


  linea_consignas_actualizada = false;
  linea_valores_actualizada = false;
  linea_alarmas_actualizada = false;

      
}


/**********************************************************************
 *          PANTALLA_STATE_INIT                                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Init (bool cambio)
{
  char buffer_texto [21];
  static int contador;
  contador ++; 


  if (cambio)
  {
      ReespiratorLCD.LCDGoto(0,0);
      ReespiratorLCD.LCDWrite("  REESPIRADOR       ", 20);

      ReespiratorLCD.LCDGoto(0,1);
      ReespiratorLCD.LCDWrite("    REESPIRADOR     ", 20);

      ReespiratorLCD.LCDGoto(0,2);
      ReespiratorLCD.LCDWrite("      REESPIRADOR   ", 20);
  }

  if ((contador%5 == 1) || cambio)
  {
      sprintf (buffer_texto, "    progreso %4i/10 ", contador/5);
      ReespiratorLCD.LCDGoto(0,3);
      ReespiratorLCD.LCDWrite(buffer_texto, 20);
  }
}


/**********************************************************************
 *          PANTALLA_STATE_IDLE                                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Idle (bool cambio)
{

  if (cambio)
  {
        ReespiratorLCD.LCDClear();

        ReespiratorLCD.LCDGoto(0,1);
        ReespiratorLCD.LCDWrite("RESPIRADOR EN REPOSO", 20);

        ReespiratorLCD.LCDGoto(0,3);
        ReespiratorLCD.LCDWrite("CONFIGURAR    marcha", 20);
  }
}




/**********************************************************************
 *          PANTALLA_STATE_CONFIGURAR_EN_REPOSO                       *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Conf_Idle (bool cambio, int edicion_linea)
{
  char buffer_texto [21];
  static int edicion_linea_ant;
  static int parametro_conf_ant;

  bool intermitencia = ((milisegundos/500) % 2 == 0);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)
  static bool intermitencia_ant;



  if ((intermitencia != intermitencia_ant) || (edicion_linea_ant != edicion_linea) || (parametro_conf_ant != parametro_conf) || cambio)
  {
    parametro_conf_ant = parametro_conf;
    edicion_linea_ant = edicion_linea;
    intermitencia_ant = intermitencia;
    
    sprintf (buffer_texto, "      consigna= %4i ", medidas [parametro_conf].valor_consigna);
        if ((edicion_linea != 1) || intermitencia)                                      // Incluye etiqueta parametro en buffer
        {
          for (int i=0; i<5; i++)
          {
            buffer_texto[i] = medidas [parametro_conf].etiqueta[i];
          }
        }    
        ReespiratorLCD.LCDGoto(0,0);
        ReespiratorLCD.LCDWrite(buffer_texto, 20);

    sprintf (buffer_texto, "      ALR_min = %4i ", medidas [parametro_conf].alarma_minimo);
        if ((edicion_linea != 2) || intermitencia)                                      // Incluye etiqueta parametro en buffer
        {
          for (int i=0; i<5; i++)
          {
            buffer_texto[i] = medidas [parametro_conf].etiqueta[i];
          }
        }    
        ReespiratorLCD.LCDGoto(0,1);
        ReespiratorLCD.LCDWrite(buffer_texto, 20);
       
    sprintf (buffer_texto, "      ALR_max = %4i ", medidas [parametro_conf].alarma_maximo);
       if ((edicion_linea != 3) || intermitencia)                                      // Incluye etiqueta parametro en buffer
        {
          for (int i=0; i<5; i++)
          {
            buffer_texto[i] = medidas [parametro_conf].etiqueta[i];
          }
        }    
        ReespiratorLCD.LCDGoto(0,2);
        ReespiratorLCD.LCDWrite(buffer_texto, 20);

              
    if (edicion_linea == 0)
    {
      ReespiratorLCD.LCDGoto(0,3);
      ReespiratorLCD.LCDWrite("cons  MIN  MAX  <<< ", 20);
    }
    else
    {
      ReespiratorLCD.LCDGoto(0,3);
      ReespiratorLCD.LCDWrite("cons  MIN  MAX   ok ", 20);
    }
  }  
}


/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC                                 *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC (bool cambio)
{
  bool intermitencia = ((milisegundos/500) % 10 == 1);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)
  static bool intermitencia_ant;
  char linea_consignas [21];

//if (cambio) Serial.println ("cambio = 1 en working_PC");


      if (linea_alarmas_actualizada || cambio)
      {
            ReespiratorLCD.LCDGoto(0,0);
            ReespiratorLCD.LCDWrite(linea_alarmas,20);
        linea_alarmas_actualizada = false;    
      }

      if (linea_consignas_actualizada || cambio)
      {       
            sprintf (linea_consignas, " %4i %4i %4i %4i\0", 
              medidas [0].valor_consigna, 
              medidas [1].valor_consigna, 
              medidas [2].valor_consigna, 
              medidas [3].valor_consigna);   
              
            ReespiratorLCD.LCDGoto(0,1);
            ReespiratorLCD.LCDWrite(linea_consignas,20);
        linea_consignas_actualizada = false;           
      }


     if (linea_valores_actualizada || cambio)
      {
            ReespiratorLCD.LCDGoto(0,2);
            ReespiratorLCD.LCDWrite(linea_valores,20);
      }


// etiquetas  
      if (cambio)
      {
        ReespiratorLCD.LCDGoto(0,3);
        ReespiratorLCD.LCDWrite(linea_etiquetas,20);
      }
      if (intermitencia != intermitencia_ant)                       // parpadea "conf"
      {
        if (intermitencia)
        {
          ReespiratorLCD.LCDGoto(15,3);
          ReespiratorLCD.LCDWrite(" conf",5);
        }
        else 
        {
           ReespiratorLCD.LCDGoto(15,3);
           ReespiratorLCD.LCDWrite(medidas [3].etiqueta,5);
        }
      }
      
  
  intermitencia_ant = intermitencia;
}



/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC_EDIT_1 (BASICO)                 *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC_edit_1 (bool cambio, int consigna_orig, int consigna_nueva)
{
  bool intermitencia = ((milisegundos/500) % 2 == 0);          //  Necesario para que parpadee la etiqueta del valor seleccionado (deshabilitado)
  static bool intermitencia_ant;
  static int consigna_nueva_ant;

  
      if (linea_alarmas_actualizada)
      {
            ReespiratorLCD.LCDGoto(0,0);
            ReespiratorLCD.LCDWrite(linea_alarmas,20);
        linea_alarmas_actualizada = false;    
      }



      // Línea de edición
      if ((intermitencia && !intermitencia_ant)||(milisegundos < milisegundos_sin_interm) || (consigna_nueva != consigna_nueva_ant) || cambio)
      {  
        char lineaEdicion[21];
        sprintf (lineaEdicion, "(%4i) => %4i ", consigna_orig, consigna_nueva); 
        ReespiratorLCD.LCDGoto(0,1);
        ReespiratorLCD.LCDWrite(medidas [boton_pulsado_seleccionado-1].etiqueta,5);
        ReespiratorLCD.LCDGoto(5,1);
        ReespiratorLCD.LCDWrite(lineaEdicion,15);
      }        
      else if (!intermitencia && intermitencia_ant)
      {
        ReespiratorLCD.LCDGoto(0, 1);
        ReespiratorLCD.LCDWrite("                    ", 20);
      }



      if (linea_valores_actualizada)
      {
        ReespiratorLCD.LCDGoto(0,2);
        ReespiratorLCD.LCDWrite(linea_valores,20);
        linea_valores_actualizada = false;    
      }


     // Gestiona la intermitencia de la etiqueta que se está editando
      
      if ((intermitencia && !intermitencia_ant) || cambio)
      {
        ReespiratorLCD.LCDGoto(5* (boton_pulsado_seleccionado-1), 3);
        ReespiratorLCD.LCDWrite("     ", 5);
      }
      else if (!intermitencia && intermitencia_ant)
      {
        ReespiratorLCD.LCDGoto(5* (boton_pulsado_seleccionado-1), 3);
        ReespiratorLCD.LCDWrite(medidas [boton_pulsado_seleccionado-1].etiqueta,5);
      }
      
  consigna_nueva_ant = consigna_nueva;
  intermitencia_ant = intermitencia;
};




/**********************************************************************
 *          PANTALLA_STATE_WORKING_PC_EDIT_2 (AVANZADO)               *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Working_PC_edit_2 (bool cambio, bool edicion, int parametro, int consigna)
{
  bool intermitencia = ((milisegundos/500) % 2 == 0);          //  Necesario para parpadear la etiqueta
  static bool intermitencia_ant;
  char lineaEdicion[21];
  static int consigna_ant, parametro_ant;

      

      if (linea_alarmas_actualizada || cambio)
      {
            ReespiratorLCD.LCDGoto(0,0);
            ReespiratorLCD.LCDWrite(linea_alarmas,20);
        linea_alarmas_actualizada = false;    
      }



      if (parametro == numParametrosSec-1)
      {
        if (consigna == 0)sprintf (lineaEdicion, "  no   ", consigna);                  // Caso especial para solicitud apagado
        else sprintf (lineaEdicion, "  si   ", consigna);        
      }
      else
      {
        if (consigna_ant != consigna) sprintf (lineaEdicion, " = %4i", consigna);
      }


      if ((intermitencia != intermitencia_ant) || (parametro != parametro_ant) || (consigna != consigna_ant) || cambio)
      {
        if ((intermitencia)||(milisegundos < milisegundos_sin_interm) || cambio)                     // imagen completa si noparpadea
        {  
          ReespiratorLCD.LCDGoto(0,1);
          ReespiratorLCD.LCDWrite(parametro_sec[parametro].etiqueta,13);
          ReespiratorLCD.LCDGoto(13,1);
          ReespiratorLCD.LCDWrite(lineaEdicion,7);
        }        
        else
        {
          if (!edicion)
          {            
            ReespiratorLCD.LCDGoto(0, 1);
            ReespiratorLCD.LCDWrite("                    ", 13);
            ReespiratorLCD.LCDGoto(13,1);
            ReespiratorLCD.LCDWrite(lineaEdicion,7);
          }
          else
          {
            ReespiratorLCD.LCDGoto(0,1);
            ReespiratorLCD.LCDWrite(parametro_sec[parametro].etiqueta,13);
            ReespiratorLCD.LCDGoto(13, 1);
            ReespiratorLCD.LCDWrite("        ", 7);  
          }
        }
      }



     if (linea_valores_actualizada || cambio)
      {
        ReespiratorLCD.LCDGoto(0,2);
        ReespiratorLCD.LCDWrite(linea_valores,20);
      }


      if (cambio)
      {
            ReespiratorLCD.LCDGoto(0,3);
            ReespiratorLCD.LCDWrite(linea_etiquetas,20);
      }

    consigna_ant = consigna;
    intermitencia_ant = intermitencia;
    parametro_ant = parametro;
    
    
};



/**********************************************************************
 *          PANTALLA_STATE_CONFIRM_GOTOIDLE                           *
 **********************************************************************/
void Panel_LCD::Pantalla_State_Confirm_gotoIdle (bool cambio)
{
  bool intermitencia = ((milisegundos/500) % 2 == 0);          //  Necesario para parpadear la etiqueta
  static bool intermitencia_ant;



    if (cambio)
    {
      ReespiratorLCD.LCDGoto(0,0);
      ReespiratorLCD.LCDWrite("                    ",20);

      ReespiratorLCD.LCDGoto(0,2);
      ReespiratorLCD.LCDWrite("                    ",20);

      ReespiratorLCD.LCDGoto(0,3);
      ReespiratorLCD.LCDWrite("  NO        si  <<< ",20);
    }


    if ((intermitencia != intermitencia_ant) || cambio)
    {            
      ReespiratorLCD.LCDGoto(0,1);
      if (intermitencia) ReespiratorLCD.LCDWrite("  PONER EN REPOSO?  ",20);
      else               ReespiratorLCD.LCDWrite("                    ",20);
    }



    intermitencia_ant = intermitencia;
};






/*
            // Cálculos asociados con los botones

            // Cálculos asociados con el encoder
                  
            // Cálculos asociados al botón del encoder

            // Transiciones asociadas con los botones
                
            // Transiciones asociadas al tiempo                  
  
            // Actualiza la pantalla

*/