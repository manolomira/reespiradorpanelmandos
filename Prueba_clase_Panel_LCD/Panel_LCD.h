#include <Encoder.h>

#include "PINOUT.h"

#include "LCDHAL.h"
#include <stdio.h>      NECESARIO PARA LCDHAL

#include "Boton_encoder.h"



#ifndef PANEL_LCD_H
#define PANEL_LCD_H



struct Parametro{     /* Struct para agrupar las variables de cada dato. Pendiente de getters y setters */
  char etiqueta[5];
  int valor_consigna;
  int valor_minimo;
  int valor_maximo;
  int valor_instantaneo;
  int valor_instantaneo_anterior;
  int valor_medio;
  int alarma_minimo;
  int alarma_maximo;
};

struct Parametro_sec{     /* Struct para agrupar las variables de cada dato. Pendiente de getters y setters */
  char etiqueta[13];
  int valor_consigna;
  int valor_minimo;
  int valor_maximo;
};

/** States of the mechanical ventilation. */
enum Estado_display {
    State_Init                    =  0,  /**< Initializing. */
    State_Idle                    =  4,  /**< Idle. */
    State_Conf_Idle               =  5,  /**< Configuración parámetros en Idle */
    State_Working_PC              = 10,  /**< Funcionamiento Control Presion (PC)*/
    State_Working_PC_edit_1       = 11,  /**< Modo PC: Edicion directa */
    State_Working_PC_edit_1_conf  = 12,  /**< Modo PC: Edicion directa_confirmacion */
    State_Working_PC_edit_2       = 14,  /**< Modo PC: Edición otros parámetros */
    State_Working_PC_edit_2_conf  = 15,  /**< Modo PC: Confirmación edicion otros parametros */
    State_Working_VC              = 20,  /**< Funcionamiento Control Volumen (VC) */
    State_Working_VC_edit_1       = 21,  /**< Modo VC: Edicion directa */
    State_Working_VC_edit_2       = 24,  /**< Modo VC: Edición otros parámetros */
    State_Confirm_gotoIdle        = 30,  /**< Confirmación parada de respirador */
    State_Confirm_gotoShutd       = 40   /**< Confirmación apagado de respirador */
};






// Clase que maneja los elementos del panel de mando con LCD
class Panel_LCD{
  
  public:





// Parametros de ajuste
    int tiempo_max_edicion = 8000;                 // es el tiempo que tarda en volver a la ventana normal
    int boton_espera_corta = 10;                   //Tiempo de filtrado de tecla (ms)
    int boton_espera_larga = 100;                  //Tiempo de pulsación larga (no implementado)   
    int TiempoFiltroEncoder = 2;                   // Tiempo de filtrado del encoder
    int intervaloRefresco = 150;                   // Determina el tiempo entre actualizaciones de la pantalla


    
    
// Constructor    
    //Panel_LCD establece los pines asociados a cada uno de los botones    
    Panel_LCD (int BUTTON1pin, int BUTTON2pin, int BUTTON3pin, int BUTTON4pin, 
               int ecoderCLKPin, int ecoderDIRPin, int ecoderBOTPin);




// Prototipos de funciones
    // init   Inicia el funcionamiento del display
    void init();

    // Configura los valores de una de las variables
//    void setParametro(int indice, char* etiqueta, int consigna, int minimo, int maximo); 

    // update     Lee los periféricos y actualiza el valor del panel en consecuencia
    void update (int valor0, int valor1, int valor2, int valor3);

// Prototipos de funciones
    int muestreo_boton_pulsado ();
    int muestreo_encoder();
    int get_lectura_encoder();
    void set_lectura_encoder (int);

    void fin_de_inicio (void);                 // Le indica al terminal que la máquina acabó de inicializarse

    void actualiza_linea_alarmas();

                                               //Pantalla_funcionamiento_xxx       Gestionan el contenido la pantalla en cada valor de estado
    void Pantalla_State_Init (bool cambio);
    void Pantalla_State_Idle (bool cambio);
    void Pantalla_State_Conf_Idle (bool cambio, int edicion_linea);
    void Pantalla_State_Working_PC (bool cambio);
    void Pantalla_State_Working_PC_edit_1 (bool cambio, int consigna_ant, int consigna_nueva);
    void Pantalla_State_Working_PC_edit_2 (bool cambio, bool edicion, int parametro, int consigna_nueva);
    void Pantalla_State_Confirm_gotoIdle (bool cambio);

    // Faltan funciones para relacionar las dos listas de parámetros o organizarlos de otra manera



// Variables  
    int _BUTTONpin[5];                            // pines de los botones. Se establecen en el constructor.
    int ultimo_boton_seleccionado;                //    ------

    int _encoderCLKpin = 16;                      // terminales del encoder
    int _encoderDIRpin = 17;
    int _encoderBOTpin = 29;
    int _encoderValor  = 0;

    bool _encoderCLK, _encoderCLK_ant;    
    bool _encoderDIR, _encoderDIR_ant;    


    
    unsigned long milisegundosRefrescoPantalla = 150;        // ¿?

    // Máuina de estados
    Estado_display estado_display;                            // estado actual del panel
    int parametro_conf = 0;

    // Variables
    Parametro medidas[10];                         // Contenedores de las variables a mostrar
    int numParametros = 6;
    Parametro_sec parametro_sec [40];
    int numParametrosSec = 10;
    

    
    int boton_pulsado_seleccionado;               // valor de la tecla pulsada y filtrada.
    unsigned long milisegundos;                   // Valor de millis al entrar en update.
    unsigned long milisegundos_ant;               // Valor anterior de milisegundos                 ¿?
    unsigned long milisegundos_reposo_edicion;    //      ------                                    ¿?

    // Variables con el contenido a mostrar de las líneas
    bool linea_consignas_actualizada = false;
    bool linea_valores_actualizada = false;
    bool linea_alarmas_actualizada = false;

    char linea_etiquetas[21];                       // Textos para la línea de etiquetas
    char linea_consignas[21];
    char linea_valores[21];
    char linea_alarmas[21];

    bool pulsado, pulsado_ant;                                    //   ¿?
    unsigned long milisegundos_sin_interm;
    
};

#endif